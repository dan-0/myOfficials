/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class ConsentActivityTest {

    @get:Rule
    val intentsTestRule = IntentsTestRule(ConsentActivity::class.java)

    @Test
    fun ensureVisible() {
        onView(withText(R.string.disclosures))
            .check(matches(isDisplayed()))
        onView(withText(R.string.consent_intro_statement))
            .check(matches(isDisplayed()))
        onView(withText(R.string.consent_privacy_info_link_text))
            .check(matches(isDisplayed()))
        onView(withText(R.string.consent_terms_info_source_link_text))
            .check(matches(isDisplayed()))
    }

    @Before
    fun stubAllExternalIntents() {
        intending(not(isInternal()))
            .respondWith(Instrumentation.ActivityResult(Activity.RESULT_OK, null))
    }

    @Test
    fun ensurePrivacyInfoLink() {
        val expected =
            allOf(hasAction(Intent.ACTION_VIEW), hasData("https://myofficials.app/privacy/"))

        onView(withText(R.string.consent_privacy_info_link_text))
            .perform(click())

        intended(expected)
    }

    @Test
    fun ensureTermsInfoLink() {
        val expected =
            allOf(hasAction(Intent.ACTION_VIEW), hasData("https://myofficials.app/terms/"))

        onView(withText(R.string.consent_terms_info_source_link_text))
            .perform(click())

        intended(expected)
    }

    @Test
    fun onAgreeExitConsent() {
        onView(withText("AGREE"))
            .perform(click())

        onView(withId(R.id.fragment_home))
            .check(matches(isDisplayed()))
    }

    @Test
    fun reopeningConsentAfterAgreeDoesNotDisplay() {
        onView(withText("AGREE"))
            .perform(click())
        onView(withId(R.id.fragment_home))
            .check(matches(isDisplayed()))

        Intent(intentsTestRule.activity, ConsentActivity::class.java).apply {
            intentsTestRule.activity.startActivity(this)
        }

        onView(withId(R.id.fragment_home))
            .check(matches(isDisplayed()))
    }
}