/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.helpers

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.matcher.BoundedMatcher
import com.idleoffice.mypoliticians.model.data.Official
import com.idleoffice.mypoliticians.ui.home.OfficialAdapter
import kotlinx.android.synthetic.main.card_official.view.*
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

object OfficialsMatchers {
    /**
     * Matcher for an [OfficialAdapter.ViewHolder] to a specific [official]
     */
    fun withOfficial(official: Official): Matcher<OfficialAdapter.ViewHolder> {
        return object : TypeSafeMatcher<OfficialAdapter.ViewHolder>() {
            override fun describeTo(description: Description?) {
                description?.appendText("with official $official")
            }

            override fun matchesSafely(item: OfficialAdapter.ViewHolder?): Boolean {
                item ?: return false

                val expectedFavoriteVisibility = if (official.isFavorite) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

                return item.itemView.isFavoriteIcon.visibility == expectedFavoriteVisibility &&
                        item.itemView.officialName.text == official.name &&
                        item.itemView.officialPosition.text == official.office
            }

        }
    }

    /**
     * Checks that a recycler view has the official provided in the matcher
     */
    fun hasOfficial(matcher: Matcher<OfficialAdapter.ViewHolder>) =
        object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {

            override fun describeTo(description: Description) {
                description.appendText("has item: ")
                matcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val adapter = view.adapter
                for (position in 0 until adapter!!.itemCount) {
                    val type = adapter.getItemViewType(position)
                    val holder = adapter.createViewHolder(view, type)
                    adapter.onBindViewHolder(holder, position)
                    if (matcher.matches(holder)) {
                        return true
                    }
                }
                return false
            }
        }
}