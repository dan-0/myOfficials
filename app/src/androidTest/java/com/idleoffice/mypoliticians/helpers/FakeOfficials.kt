/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.helpers

import com.idleoffice.mypoliticians.model.data.*

object FakeOfficials {
    private val singleFakeOfficial = Official(
        1,
        name = "Fake Official 1",
        office = "President",
        party = "Demoican",
        address = listOf(OfficialContactData(ContactType.ADDRESS, "123 fake Stree")),
        phones = listOf(OfficialContactData(ContactType.PHONE, "555-555-5555")),
        emails = listOf(OfficialContactData(ContactType.EMAIL, "fakemail@fakemail.com")),
        urls = listOf(OfficialContactData(ContactType.URL, "https://myofficials.app")),
        twitterChannel = Channel("@DanLowe0x00", ChannelType.TWITTER),
        facebookChannel = Channel("CSPAN", ChannelType.FACEBOOK),
        youtubeChannel = Channel("CSPAN", ChannelType.YOUTUBE),
        photoUrl = null,
        isFavorite = false
    )

    val basicFakeOfficials = mutableListOf(
        singleFakeOfficial,
        singleFakeOfficial.copy(
            id = 2,
            name = "Fake Official 2",
            office = "Vice-President",
            party = "Demoican"
        )
    )

    fun generateFakeOfficials(numberToGenerate: Int): MutableList<Official> {
        val officials = mutableListOf<Official>()

        repeat(numberToGenerate) {
            officials.add(
                singleFakeOfficial.copy(
                    id = it,
                    name = "Fake official $it",
                    office = "office of $it")
            )
        }

        return officials
    }
}