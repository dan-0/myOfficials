/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home

import android.view.View
import androidx.annotation.StringRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToHolder
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import com.idleoffice.mypoliticians.R
import com.idleoffice.mypoliticians.helpers.OfficialsMatchers.withOfficial
import com.idleoffice.mypoliticians.helpers.RecyclerViewAssertions.hasElementCount
import com.idleoffice.mypoliticians.helpers.RecyclerViewMatcher.Companion.withRecyclerView
import com.idleoffice.mypoliticians.helpers.runOnCurrentActivity
import com.idleoffice.mypoliticians.model.data.Official
import org.hamcrest.Matchers.*

class HomeFragmentRobot private constructor() {

    private val runner = Do()
    private val checker = Verify()

    companion object {
        fun homeRobot(
            func: HomeFragmentRobot.() -> Unit
        ) = HomeFragmentRobot().apply {
            func()
        }
    }

    fun doAction(func: Do.() -> Unit) = runner.apply {
        func()
    }

    fun verify(func: Verify.() -> Unit) = checker.apply {
        func()
    }



    class Do {
        /**
         * Type the [address] into the address search
         */
        fun typeAddressIntoSearch(address: String) = apply {
            onView(withId(R.id.addressText))
                .perform(typeText(address))
        }

        /**
         * Tap the IME search button on the keypad
         */
        fun tapKeypadSearchButton() = apply {
            onView(withId(R.id.addressText))
                .perform(pressImeActionButton())
        }

        /**
         * Tap the provided [official] in the list
         */
        fun tapOfficial(official: Official) = apply {
            onView(withId(R.id.officialsList))
                .perform(scrollToHolder(withOfficial(official)))

            onView(withText(official.name))
                .perform(click())
        }

        /**
         * Tap the favorites tab
         */
        fun tapFavorites() = apply {
            onView(withId(R.id.action_favorite))
                .perform(click())
        }

        /**
         * Scroll to a numeric [scrollPosition] position in the ViewHolder
         */
        fun scrollToHolderPosition(scrollPosition: Int) = apply {
            onView(withId(R.id.officialsList))
                .perform(
                    scrollToPosition<OfficialAdapter.ViewHolder>(scrollPosition)
                )
        }
    }

    class Verify {
        /**
         * Check that the given toast string [id] is displayed
         */
        fun toastText(@StringRes id: Int) = apply {
            val decorView = getInstrumentation().runOnCurrentActivity<View> { window.decorView }

            onView(withText(id))
                .inRoot(withDecorView(not(`is`(decorView))))
                .check(matches(isDisplayed()))
        }

        /**
         * Check that the "No Results" view is displayed. Indicates that either there are no officials
         * for the search or that a search was not made.
         */
        fun noResultsViewIsDisplayed() = apply {
            onView(
                allOf(
                    withId(R.id.noResultsText),
                    withText(R.string.no_politician_results)
                )
            ).check(matches(isDisplayed()))

        }

        /**
         * Checks to ensure that the "No Results" view is not currently being displayed
         */
        fun noResultsViewNotDisplayed() = apply {
            onView(
                allOf(
                    withId(R.id.noResultsText),
                    withText(R.string.no_politician_results)
                )
            ).check(matches(not(isDisplayed())))

        }

        /**
         * Check the number of officials [totalOfficials] in the current RecyclerView
         */
        fun numberOfOfficials(totalOfficials: Int) = apply {
            onView(withId(R.id.officialsList))
                .check(hasElementCount(totalOfficials))
        }

        /**
         * Check that the [Official] at [position] matches [official]
         */
        fun officialAtPositionMatches(position: Int, official: Official) = apply {
            onView(withRecyclerView(R.id.officialsList).atPosition(position))
                .check(matches(hasDescendant(withText(official.name))))
                .check(matches(hasDescendant(withText(official.office))))
        }

        /**
         * Checks if [official] is in the list based on if the official [shouldBeInList]
         */
        fun officialInList(official: Official) = apply {
            onView(withId(R.id.officialsList))
                .perform(
                    scrollToHolder(withOfficial(official))
                )
                .check(matches(isDisplayed()))
        }

        /**
         * Check that the last address search text matches the expected [lastAddress]
         */
        fun lastAddressMatches(lastAddress: String) = apply {
            onView(withId(R.id.lastUserAddress))
                .check(matches(allOf(isDisplayed(), withText(lastAddress))))
        }

        /**
         * Ensures the [official] is in the list
         */
        fun officialIsFavorite(official: Official) = apply {
            val expectedOfficial = official.copy(isFavorite = true)
            onView(withId(R.id.officialsList))
                .perform(scrollToHolder(withOfficial(expectedOfficial)))
        }

        /**
         * Ensure the [official] is visible in the current view without scrolling
         */
        fun officialIsVisible(official: Official) = apply {
            onView(withText(official.name))
                .check(matches(isDisplayed()))
        }
    }
}