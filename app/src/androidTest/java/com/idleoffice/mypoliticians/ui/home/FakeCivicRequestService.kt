/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home

import com.idleoffice.mypoliticians.retrofit.CivicRequestService
import com.idleoffice.mypoliticians.ui.base.mvi.SearchResultState

class FakeCivicRequestService(private val resultStates: List<SearchResultState>) : CivicRequestService {

    private var nextState = 0

    override suspend fun getRepresentativesAsync(address: String): SearchResultState {
        val next = if (nextState >= resultStates.lastIndex) {
            resultStates.lastIndex
        } else {
            nextState++
        }
        return resultStates[next]
    }
}