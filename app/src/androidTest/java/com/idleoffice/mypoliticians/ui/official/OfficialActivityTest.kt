/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.official

import android.content.Intent
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.intent.rule.IntentsTestRule
import com.idleoffice.mypoliticians.idlingresource.AppIdlingResource
import com.idleoffice.mypoliticians.model.data.*
import com.idleoffice.mypoliticians.ui.official.OfficialActivityRobot.Companion.officialRobot
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.get

class OfficialActivityTest : KoinTest {

    @get:Rule
    val activityTestRule = IntentsTestRule(OfficialActivity::class.java, false, false)

    private lateinit var appIdlingResource: AppIdlingResource

    private val testOfficial = Official(
        0,
        "Test Official", "President", "Green",
        listOf(
            OfficialContactData(ContactType.ADDRESS, "123 Fake Street, Washington, DC 20021"),
            OfficialContactData(ContactType.ADDRESS, "321 Faker Street, New York, NY 20001")
        ),
        listOf(
            OfficialContactData(ContactType.PHONE, "202-123-4567"),
            OfficialContactData(ContactType.PHONE, "+1 (206) 867-5309")
        ),
        listOf(
            OfficialContactData(ContactType.EMAIL, "testofficial@testofficial.com"),
            OfficialContactData(ContactType.EMAIL, "testofficial@test.gov")
        ),
        listOf(
            OfficialContactData(ContactType.URL, "https://whitehouse.gov"),
            OfficialContactData(ContactType.URL, "myofficials.app")
        ),
        Channel("@DanLowe0x00", ChannelType.TWITTER),
        Channel("whitehouse", ChannelType.FACEBOOK),
        Channel("whitehouse", ChannelType.YOUTUBE),
        null,
        false

    )

    @Before
    fun setUp() {
        appIdlingResource = get()
        IdlingRegistry.getInstance().register(appIdlingResource)
    }

    @After
    fun cleanUp() {
        IdlingRegistry.getInstance().unregister(appIdlingResource)
    }

    // all official data is populated
    @Test
    fun checkAllDataIsPopulated() {

        robot(testOfficial) {
            verify {
                photoIsDisplayed()
                officeIsDisplayed(testOfficial.office)
                nameIsDisplayed(testOfficial.name)
                partyIsDisplayed(testOfficial.party!!)
                addressIsDisplayed(testOfficial.address!![0].value)
                addressIsDisplayed(testOfficial.address!![1].value)
                phoneNumberIsDisplayed(testOfficial.phones!![0].value)
                phoneNumberIsDisplayed(testOfficial.phones!![1].value)
                urlIsDisplayed(testOfficial.urls!![0].value)
                urlIsDisplayed(testOfficial.urls!![1].value)
                twitterIsDisplayed()
                facebookIsDisplayed()
                youtubeIsDisplayed()
                officialNotFavorite()
            }
        }
    }

    @Test
    fun emptyPartyNotDisplayed() {
        val testOfficial = testOfficial.copy(party = null)
        robot(testOfficial) {
            verify {
                partyNotDisplayed()
            }
        }
    }

    @Test
    fun favoriteButtonSetsToFavorite() {
        robot(testOfficial) {
            doAction {
                tapFavorite()
            }

            verify {
                officialIsFavorite()
            }
        }
    }

    @Test
    fun favoriteButtonUndoesFavorite() {
        val expectedOfficial = testOfficial.copy(isFavorite = true)

        robot(expectedOfficial) {

            doAction {
                tapFavorite()
            }

            verify {
                officialNotFavorite()
            }

        }
    }

    @Test
    fun addressTapLaunches() {
        val expectedAddress = testOfficial.address!![0].value

        robot(testOfficial) {
            doAction {
                tapAddress(expectedAddress)
            }
            verify {
                addressIntentReceived(expectedAddress)
            }
        }
    }

    @Test
    fun emailTapLaunches() {
        val expectedEmail = testOfficial.emails!![0].value

        robot(testOfficial) {
            doAction {
                tapEmail(expectedEmail)
            }

            verify {
                verifyEmailIntent(expectedEmail)
            }
        }
    }

    @Test
    fun phoneNumberTapLaunches() {
        val expectedNumber = testOfficial.phones!![0].value

        robot(testOfficial) {
            doAction {
                tapPhoneNumber(expectedNumber)
            }

            verify {
                phoneIntentReceived(expectedNumber)
            }
        }
    }

    @Test
    fun urlTapTriggersLaunch() {
        val expectedUrl = testOfficial.urls!![0].value

        robot(testOfficial) {
            doAction {
                tapUrl(expectedUrl)
            }

            verify {
                urlIntentReceived(expectedUrl)
            }
        }
    }

    @Test
    fun youtubeTapTriggersLaunch() {
        val youtubeName = testOfficial.youtubeChannel!!.id!!

        robot(testOfficial) {
            doAction {
                tapYoutube(youtubeName)
            }

            verify {
                youtubeIntentReceived(youtubeName)
            }
        }
    }

    @Test
    fun emptyYoutubeNotDisplayed() {
        val testOfficial = testOfficial.copy(youtubeChannel = null)

        robot(testOfficial) {
            verify {
                youtubeNotDisplayed()
            }
        }
    }

    @Test
    fun twitterTapTriggersLaunch() {
        val twitterName = testOfficial.twitterChannel!!.id!!

        robot(testOfficial) {
            doAction {
                tapTwitter(twitterName)
            }

            verify {
                twitterIntentReceived(twitterName)
            }
        }
    }

    @Test
    fun emptyTwitterNotDisplayed() {
        val testOfficial = testOfficial.copy(twitterChannel = null)
        robot(testOfficial) {
            verify {
                twitterNotDisplayed()
            }
        }
    }

    @Test
    fun facebookTapTriggersLaunch() {
        robot(testOfficial) {
            val facebookName = testOfficial.facebookChannel!!.id!!

            doAction {
                tapFacebookIcon(facebookName)
            }

            verify {
                facebookIntentReceived(facebookName)
            }
        }
    }

    @Test
    fun emptyFacebookNotDisplayed() {
        val testOfficial = testOfficial.copy(facebookChannel = null)
        robot(testOfficial) {
            verify {
                facebookNotDisplayed()
            }
        }
    }
    
    private fun robot(
        testOfficial: Official?,
        func: OfficialActivityRobot.() -> Unit
    ): OfficialActivityRobot {
        val startIntent = Intent().apply {
            testOfficial?.let {
                putExtra(OfficialActivity.OFFICIAL_DATA, it)
            }
        }
        activityTestRule.launchActivity(startIntent)
        return officialRobot(
            func
        )
    }
}