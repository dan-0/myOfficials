/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui

import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.intent.rule.IntentsTestRule
import com.idleoffice.mypoliticians.helpers.FakeOfficials
import com.idleoffice.mypoliticians.idlingresource.AppIdlingResource
import com.idleoffice.mypoliticians.model.data.Official
import com.idleoffice.mypoliticians.model.favorite.FavoritesProvider
import com.idleoffice.mypoliticians.ui.OfficialIntegrationTestRobot.Companion.integrationRobot
import com.idleoffice.mypoliticians.ui.base.mvi.SearchResultState
import com.idleoffice.mypoliticians.ui.favorite.FavoriteFragmentRobot.Companion.favoriteRobot
import com.idleoffice.mypoliticians.ui.home.HomeFragmentRobot.Companion.homeRobot
import com.idleoffice.mypoliticians.ui.home.HomeFragmentSetup
import com.idleoffice.mypoliticians.ui.main.MainActivity
import com.idleoffice.mypoliticians.ui.official.OfficialActivityRobot.Companion.officialRobot
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.get

class OfficialsIntegrationTest : KoinTest {

    @get:Rule
    val activityTestRule = IntentsTestRule(MainActivity::class.java, false, false)

    private lateinit var appIdlingResource: AppIdlingResource

    @Before
    fun setUp() {
        appIdlingResource = get()
        IdlingRegistry.getInstance().register(appIdlingResource)
    }

    @After
    fun cleanUp() {
        IdlingRegistry.getInstance().unregister(appIdlingResource)
    }

    @Test
    fun ensureFavoriteOfficialSaved() {
        val fakeOfficials = FakeOfficials.basicFakeOfficials
        val expectedOfficial = fakeOfficials[0].copy(isFavorite = true)

        integrationRobot {

            homeRobot {
                doAction {
                    typeAddressIntoSearch("123 Fake Street")
                    tapKeypadSearchButton()
                    tapOfficial(fakeOfficials[0])
                }
            }

            officialRobot {
                doAction {
                    tapFavorite()
                    tapBack()
                }
            }

            homeRobot {
                verify {
                    officialInList(expectedOfficial)
                }
            }
        }
    }

    @Test
    fun ensureSavedFavoriteIsShownInQuery() {
        val targetOfficial = FakeOfficials.basicFakeOfficials[0]

        // Add official to favorite before start
        addOfficialToFavorites(targetOfficial)

        integrationRobot {
            homeRobot {
                doAction {
                    typeAddressIntoSearch("11111")
                    tapKeypadSearchButton()
                }
                verify {
                    officialIsFavorite(targetOfficial)
                }
            }
        }
    }

    @Test
    fun ensureAddingOfficialAsFavoriteFromOfficialActivity() {
        val fakeOfficials = FakeOfficials.basicFakeOfficials
        val targetOfficial = fakeOfficials[0]

        integrationRobot {
            homeRobot {
                doAction {
                    typeAddressIntoSearch("11111")
                    tapKeypadSearchButton()
                    tapOfficial(targetOfficial)
                }
            }

            officialRobot {
                doAction {
                    tapFavorite()
                    tapBack()
                }
            }

            homeRobot {
                doAction {
                    tapFavorites()
                }
            }

            favoriteRobot {
                verify {
                    numberOfFavorites(1)
                    officialsInFavorites(mutableListOf(targetOfficial))
                }
            }
        }
    }

    @Test
    fun removeOfficialFromFavoritesInOfficialViewPersists() {
        val fakeOfficials = FakeOfficials.basicFakeOfficials
        val targetOfficial = fakeOfficials[0]

        integrationRobot {
            homeRobot {
                doAction {
                    typeAddressIntoSearch("11111")
                    tapKeypadSearchButton()
                    tapOfficial(targetOfficial)
                }
            }

            officialRobot {
                doAction {
                    tapFavorite()
                    tapBack()
                }
            }

            homeRobot {
                doAction {
                    tapFavorites()
                }
            }

            favoriteRobot {
                verify {
                    numberOfFavorites(1)
                    officialsInFavorites(mutableListOf(targetOfficial))
                }

                doAction {
                    tapHome()
                }
            }

            homeRobot {
                doAction {
                    tapOfficial(targetOfficial.copy(isFavorite = true))
                }
            }

            officialRobot {
                doAction {
                    tapFavorite()
                    tapBack()
                }
            }

            homeRobot {
                doAction {
                    tapFavorites()
                }
            }

            favoriteRobot {
                verify {
                    numberOfFavorites(0)
                }
            }
        }
    }

    private fun integrationRobot(
        resultState: SearchResultState.Content? = null,
        func: OfficialIntegrationTestRobot.() -> Unit
    ): OfficialIntegrationTestRobot {

        val appliedResultState = resultState ?: getStandardResultState()

        HomeFragmentSetup(this)
            .setup(null, listOf(appliedResultState), activityTestRule)
        return integrationRobot(func)
    }

    private fun addOfficialToFavorites(official: Official) {
        val favoritesProvider: FavoritesProvider = get()
        runBlocking {
            favoritesProvider.addFavoriteOfficial(official)
        }
    }

    private fun getStandardResultState(): SearchResultState {
        val fakeOfficials = FakeOfficials.basicFakeOfficials
        return SearchResultState.Content(fakeOfficials)
    }
}