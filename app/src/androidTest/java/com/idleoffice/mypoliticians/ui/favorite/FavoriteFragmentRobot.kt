/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.favorite

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToHolder
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.idleoffice.mypoliticians.R
import com.idleoffice.mypoliticians.helpers.OfficialsMatchers
import com.idleoffice.mypoliticians.helpers.OfficialsMatchers.withOfficial
import com.idleoffice.mypoliticians.helpers.RecyclerViewAssertions
import com.idleoffice.mypoliticians.model.data.Official

class FavoriteFragmentRobot {

    private val runner = Do()
    private val verifier = Verify()

    companion object {
        fun favoriteRobot(
            func: FavoriteFragmentRobot.() -> Unit
        ) = FavoriteFragmentRobot().apply {
            func()
        }
    }

    fun doAction(func: Do.() -> Unit) = runner.apply {
        func()
    }

    fun verify(func: Verify.() -> Unit) = verifier.apply {
        func()
    }

    class Do {

        /**
         * Remove a favorite official from the list
         */
        fun removeFavorite(official: Official) = apply {
            onView(withId(R.id.officialsList))
                .perform(scrollToHolder(withOfficial(official)))

            onView(ViewMatchers.withText(official.name))
                .check(matches(ViewMatchers.isDisplayed()))
                .perform(click())

            onView(withId(R.id.favoriteFab))
                .perform(click())

            pressBack()
        }

        /**
         * Tap the home tab
         */
        fun tapHome() {
            onView(withId(R.id.action_home))
                .perform(click())
        }
    }

    class Verify {
        /**
         * Check that there are a given [number] of officials in the Favorite list
         */
        fun numberOfFavorites(number: Int) = apply {
            onView(withId(R.id.officialsList))
                .check(RecyclerViewAssertions.hasElementCount(number))
        }

        /**
         * Ensure all [officials] are listed as favorites
         */
        fun officialsInFavorites(officials: MutableList<Official>) = apply {
            onView(withId(R.id.officialsList)).apply {
                officials.map { it.copy(isFavorite = true) } .forEach {
                    perform(scrollToHolder(withOfficial(it)))
                        .check(matches(OfficialsMatchers.hasOfficial(withOfficial(it))))
                }
            }
        }
    }
}