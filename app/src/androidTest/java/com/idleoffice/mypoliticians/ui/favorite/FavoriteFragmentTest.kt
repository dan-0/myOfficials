/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.favorite

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.IdlingRegistry
import com.idleoffice.mypoliticians.R
import com.idleoffice.mypoliticians.helpers.FakeOfficials
import com.idleoffice.mypoliticians.idlingresource.AppIdlingResource
import com.idleoffice.mypoliticians.model.data.Official
import com.idleoffice.mypoliticians.model.favorite.FavoritesProvider
import com.idleoffice.mypoliticians.ui.favorite.FavoriteFragmentRobot.Companion.favoriteRobot
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.get

class FavoriteFragmentTest : KoinTest {

    private lateinit var appIdlingResource: AppIdlingResource

    @Before
    fun setUp() {
        appIdlingResource = get()
        IdlingRegistry.getInstance().register(appIdlingResource)
    }

    @After
    fun cleanUp() {
        IdlingRegistry.getInstance().unregister(appIdlingResource)
    }

    @Test
    fun noOfficialsWhenPristine() {
        robot {
            verify {
                numberOfFavorites(0)
            }
        }
    }

    @Test
    fun ensureExistingFavoritePopulated() {
        val numOfOfficials = 1
        val fakeOfficials = FakeOfficials.generateFakeOfficials(numOfOfficials)
            .map { it.copy(isFavorite = true) }
            .toMutableList()

        robot(fakeOfficials) {
            verify {
                numberOfFavorites(numOfOfficials)
                officialsInFavorites(fakeOfficials)
            }
        }
    }

    @Test
    fun ensureFavoriteIsRemoved() {
        val numOfOfficials = 1
        val fakeOfficials = FakeOfficials.generateFakeOfficials(numOfOfficials)
            .map { it.copy(isFavorite = true) }
            .toMutableList()

        robot(fakeOfficials) {
            doAction {
                removeFavorite(fakeOfficials[0])
            }

            verify {
                numberOfFavorites(0)
            }
        }
    }

    private fun robot(
        officials: MutableList<Official> = mutableListOf(),
        func: FavoriteFragmentRobot.() -> Unit
    ) {
        RobotHelper(this)
            .startWithArgs(officials)

        favoriteRobot(func)
    }

    private class RobotHelper(private val koinTest: KoinTest) {
        fun startWithArgs(officials: MutableList<Official>) {
            val favoritesProvider: FavoritesProvider = koinTest.get()
            officials.forEach {
                runBlocking {
                    favoritesProvider.addFavoriteOfficial(it)
                }
            }

            launchFragmentInContainer<FavoriteFragment>(null, R.style.AppTheme_NoActionBar)
        }

    }
}