/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home

import android.content.SharedPreferences
import androidx.test.rule.ActivityTestRule
import com.idleoffice.mypoliticians.retrofit.CivicRequestService
import com.idleoffice.mypoliticians.ui.base.mvi.SearchResultState
import com.idleoffice.mypoliticians.ui.main.MainActivity
import org.koin.test.KoinTest
import org.koin.test.get
import org.koin.test.mock.declare

/**
 * Handles setup of the Home Fragment
 */
class HomeFragmentSetup(private val koinTest: KoinTest) {
    fun setup(
        lastAddress: String? = null,
        resultStates: List<SearchResultState>? = null,
        activityTestRule: ActivityTestRule<MainActivity>
    ) {
        if (resultStates != null) {
            setPreconditions(lastAddress, resultStates, activityTestRule)
        }
        activityTestRule.launchActivity(null)
    }

    private fun setPreconditions(
        address: String?,
        resultStates: List<SearchResultState>,
        activityTestRule: ActivityTestRule<*>
    ) {
        address?.let {
            setLastAddress(address, activityTestRule)
        }

        setCivicServiceRequestResult(resultStates, activityTestRule)
    }

    private fun setCivicServiceRequestResult(
        resultState: List<SearchResultState>,
        activityTestRule: ActivityTestRule<*>
    ) {
        activityTestRule.runOnUiThread {
            koinTest.declare {
                single(override = true) { FakeCivicRequestService(resultState) as CivicRequestService }
            }
        }
    }

    private fun setLastAddress(
        address: String,
        activityTestRule: ActivityTestRule<*>
    ) {
        activityTestRule.runOnUiThread {
            val sharedPreferences: SharedPreferences = koinTest.get()
            sharedPreferences.edit()
                .putString(HomeViewModel.HOME_ADDRESS_KEY, address)
                .commit()
        }
    }
}