/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.official

import android.app.Instrumentation
import android.content.Intent
import android.net.Uri
import android.widget.TextView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToHolder
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasData
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import com.idleoffice.mypoliticians.R
import org.hamcrest.Description
import org.hamcrest.Matchers.*

/**
 * Robot for interacting with the [OfficialActivity]
 */
class OfficialActivityRobot private constructor() {

    private val runner = Do()
    private val verifier = Verify()

    companion object {
        /**
         * Load the Robot for interacting with the [OfficialActivity]
         */
        fun officialRobot(
            func: OfficialActivityRobot.() -> Unit
        ) = OfficialActivityRobot().apply {
            func()
        }
    }

    fun doAction(func: Do.() -> Unit) = runner.apply {
        func()
    }

    fun verify(func: Verify.() -> Unit) = verifier.apply {
        func()
    }

    class Do {
        /**
         * Tap the provided [address]. Handles stubbing the Intent for displaying the address.
         */
        fun tapAddress(address: String) = apply {
            intending(
                allOf(
                    hasAction(Intent.ACTION_VIEW),
                    hasData(Uri.parse("geo:0,0?q=$address"))
                )
            ).respondWith(Instrumentation.ActivityResult(0, Intent()))

            onView(withId(R.id.officialContactList))
                .perform(scrollToHolder(Helper.contactMatcher(address)))

            onView(withText(address))
                .tap()
        }



        /**
         * Tap the provided [emailAddress]. Handles stubbing the Intent for emailing the emailAddress.
         */
        fun tapEmail(emailAddress: String) = apply {
            intending(
                allOf(
                    hasAction(Intent.ACTION_SENDTO),
                    hasData(Uri.parse("mailto:$emailAddress"))
                )
            ).respondWith(Instrumentation.ActivityResult(0, Intent()))

            onView(withId(R.id.officialContactList))
                .perform(scrollToHolder(Helper.contactMatcher(emailAddress)))

            onView(withText(emailAddress))
                .tap()
        }



        /**
         * Tap the given [phoneNumber]. Handles stubbing the Intent for calling the phone number.
         */
        fun tapPhoneNumber(phoneNumber: String) = apply {
            intending(
                allOf(
                    hasAction(Intent.ACTION_DIAL),
                    hasData(Uri.parse("tel:$phoneNumber"))
                )
            ).respondWith(Instrumentation.ActivityResult(0, Intent()))

            onView(withId(R.id.officialContactList))
                .perform(scrollToHolder(Helper.contactMatcher(phoneNumber)))

            onView(withText(phoneNumber))
                .tap()
        }

        /**
         * Tap the given [url]. Handles stubbing the Intent associated with launching the [url]
         */
        fun tapUrl(url: String) = apply {
            intending(
                allOf(
                    hasAction(Intent.ACTION_VIEW),
                    hasData(Uri.parse(url))
                )
            ).respondWith(Instrumentation.ActivityResult(0, Intent()))

            onView(withId(R.id.officialContactList))
                .perform(scrollToHolder(Helper.contactMatcher(url)))
            onView(withText(url))
                .tap()
        }


        /**
         * Tap the Twitter social media icon associated with the given [twitterHandle]. Handles
         * stubbing the Intent associated with launching Twitter
         */
        fun tapTwitter(twitterHandle: String) = apply {
            val twitterUrl = Helper.resolveTwitterPath(twitterHandle)

            intending(
                allOf(
                    hasAction(Intent.ACTION_VIEW),
                    hasData(Uri.parse(twitterUrl))
                )
            ).respondWith(Instrumentation.ActivityResult(0, Intent()))

            onView(withId(R.id.bottom_of_scroll))
                .perform(scrollTo())

            onView(withId(R.id.twitterIcon))
                .tap()
        }


        /**
         * Tap the Youtube social media icon associated with the given [youtubeHandle]. Handles
         * stubbing the Intent associated with launching Youtube
         */
        fun tapYoutube(youtubeHandle: String) = apply {
            val youtubePath = Helper.resolveYoutubePath(youtubeHandle)

            intending(
                allOf(
                    hasAction(Intent.ACTION_VIEW),
                    hasData(Uri.parse("https://youtube.com/$youtubePath"))
                )
            ).respondWith(Instrumentation.ActivityResult(0, Intent()))

            onView(withId(R.id.bottom_of_scroll))
                .perform(scrollTo())

            onView(withId(R.id.youtubeIcon))
                .tap()
        }

        /**
         * Tap the Facebook icon associated with the given [facebookName]. Handles stubbing the
         * Intent for launching Facebook
         */
        fun tapFacebookIcon(facebookName: String) = apply {
            intending(
                allOf(
                    hasAction(Intent.ACTION_VIEW),
                    hasData(Uri.parse("https://facebook.com/$facebookName"))
                )
            ).respondWith(Instrumentation.ActivityResult(0, Intent()))

            onView(withId(R.id.bottom_of_scroll))
                .perform(scrollTo())

            onView(withId(R.id.facebookIcon))
                .tap()
        }

        /**
         * Tap Favorite button
         */
        fun tapFavorite() = apply {
            onView(withId(R.id.favoriteFab))
                .tap()
        }

        fun tapBack() = apply {
            pressBack()
        }

        private fun ViewInteraction.tap() = perform(click())
    }
    
    class Verify {
        
        /**
         * Check that the official photo is displayed
         */
        fun photoIsDisplayed() = apply {
            onView(withId(R.id.officialImageLayout))
                .check(matches(isDisplayed()))
        }

        /**
         * Check the current official's [office] is displayed
         */
        fun officeIsDisplayed(office: String) = apply {
            onView(
                allOf(
                    withId(R.id.officialTitle),
                    withText(office)
                )
            ).checkIsDisplayed()
        }

        /**
         * Check that the current official's [name] is displayed
         */
        fun nameIsDisplayed(name: String) = apply {
            onView(
                allOf(
                    withId(R.id.officialName),
                    withText(name)
                )
            )
                .checkIsDisplayed()
        }

        /**
         * Check that the given [party] is displayed for the current official
         */
        fun partyIsDisplayed(party: String) = apply {
            onView(
                allOf(
                    withId(R.id.officialParty),
                    withText(party)
                )
            ).checkIsDisplayed()
        }

        /**
         * Check that a party isn't displayed for the current official
         */
        fun partyNotDisplayed() = apply {
            onView(allOf(
                withId(R.id.officialParty),
                withText(""))
            ).checkIsDisplayed()
        }

        /**
         * Check that the given [address] is displayed
         */
        fun addressIsDisplayed(address: String) = apply {
            onView(withId(R.id.officialContactList))
                .perform(scrollToHolder(Helper.contactMatcher(address)))
        }

        /**
         * Verify that the Intent to load the given [emailAddress] was received
         */
        fun verifyEmailIntent(emailAddress: String) = apply {
            intended(
                allOf(
                    hasAction(Intent.ACTION_SENDTO),
                    hasData(Uri.parse("mailto:$emailAddress"))
                )
            )
        }

        /**
         * Verify that the Intent to load the given [address] was received
         */
        fun addressIntentReceived(address: String) = apply {
            intended(
                allOf(
                    hasAction(Intent.ACTION_VIEW),
                    hasData(Uri.parse("geo:0,0?q=$address"))
                )
            )
        }

        /**
         * Check that the given [phoneNumber] for the official is displayed
         */
        fun phoneNumberIsDisplayed(phoneNumber: String) = apply {
            onView(withId(R.id.officialContactList))
                .perform(scrollToHolder(Helper.contactMatcher(phoneNumber)))
        }
        
        /**
         * Verify the Intent to call the [phoneNumber] was received
         */
        fun phoneIntentReceived(phoneNumber: String) = apply {
            intended(
                allOf(
                    hasAction(Intent.ACTION_DIAL),
                    hasData(Uri.parse("tel:$phoneNumber"))
                )
            )
        }

        /**
         * Check that the provided [url] is displayed
         */
        fun urlIsDisplayed(url: String) = apply {
            onView(withId(R.id.officialContactList))
                .perform(scrollToHolder(Helper.contactMatcher(url)))
        }


        /**
         * Verify that an Intent was received to launch the [url]
         */
        fun urlIntentReceived(url: String) = apply {
            intended(
                allOf(
                    hasAction(Intent.ACTION_VIEW),
                    hasData(Uri.parse(url))
                )
            )
        }

        /**
         * Check that the Twitter social media icon is displayed
         */
        fun twitterIsDisplayed() = apply {
            onView(withId(R.id.bottom_of_scroll))
                .perform(scrollTo())

            onView(withId(R.id.twitterIcon))
                .checkIsDisplayed()
        }

        /**
         * Ensure that the Twitter icon is not displayed
         */
        fun twitterNotDisplayed() = apply {
            onView(withId(R.id.socialMediaLayout))
                .perform(scrollTo())

            onView(withId(R.id.twitterIcon))
                .checkIsNotDisplayed()
        }


        /**
         * Validates an Intent to launch the given [twitterHandle] was received
         */
        fun twitterIntentReceived(twitterHandle: String) = apply {
            val twitterUrl = Helper.resolveTwitterPath(twitterHandle)
            intended(
                allOf(
                    hasAction(Intent.ACTION_VIEW),
                    hasData(Uri.parse(twitterUrl))
                )
            )
        }

        /**
         * Check that the Youtube icon is displayed
         */
        fun youtubeIsDisplayed() = apply {
            onView(withId(R.id.bottom_of_scroll))
                .perform(scrollTo())

            onView(withId(R.id.youtubeIcon))
                .checkIsDisplayed()
        }

        /**
         * Ensures the Youtube icon is not displayed
         */
        fun youtubeNotDisplayed() = apply {
            onView(withId(R.id.bottom_of_scroll))
                .perform(scrollTo())

            onView(withId(R.id.youtubeIcon))
                .checkIsNotDisplayed()
        }


        /**
         * Validates an Intent was received to launch the given [youtubeHandle]
         */
        fun youtubeIntentReceived(youtubeHandle: String) = apply {
            val youtubePath = Helper.resolveYoutubePath(youtubeHandle)

            intended(
                allOf(
                    hasAction(Intent.ACTION_VIEW),
                    hasData(Uri.parse("https://youtube.com/$youtubePath"))
                )
            )
        }

        /**
         * Check that the Facebook social media icon is displayed
         */
        fun facebookIsDisplayed() = apply {
            onView(withId(R.id.bottom_of_scroll))
                .perform(scrollTo())

            onView(withId(R.id.facebookIcon))
                .checkIsDisplayed()
        }

        /**
         * Check that the Facebook social media icon is not displayed
         */
        fun facebookNotDisplayed() = apply {
            onView(withId(R.id.bottom_of_scroll))
                .perform(scrollTo())

            onView(withId(R.id.facebookIcon))
                .checkIsNotDisplayed()
        }

        /**
         * Validates that an Intent was received to launch the given [facebookName]
         */
        fun facebookIntentReceived(facebookName: String) = apply {
            intended(
                allOf(
                    hasAction(Intent.ACTION_VIEW),
                    hasData(Uri.parse("https://facebook.com/$facebookName"))
                )
            )
        }

        /**
         * Check that the current official is a Favorite
         */
        fun officialIsFavorite() = apply {
            onView(withId(R.id.favoriteFab))
                .check(matches(withTagValue(`is`("isFavorite"))))
        }

        /**
         * Check that the current official is not a Favorite
         */
        fun officialNotFavorite() = apply {
            onView(withId(R.id.favoriteFab))
                .check(matches(withTagValue(`is`("notFavorite"))))
        }

        private fun ViewInteraction.checkIsDisplayed() =
            check(matches(isDisplayed()))

        private fun ViewInteraction.checkIsNotDisplayed() =
            check(matches(not(isDisplayed())))
    }
    
    private object Helper {
        fun resolveTwitterPath(twitterHandle: String): String {

            val twitterEnabled = runCatching {
                getInstrumentation()
                    .context
                    .packageManager
                    .getApplicationInfo("com.twitter.android", 0)
                    .enabled
            }.getOrElse { false }

            return if (twitterEnabled) {
                "twitter://user?screen_name=$twitterHandle"
            } else {
                "https://twitter.com/$twitterHandle"
            }
        }

        fun resolveYoutubePath(youtubeHandle: String): String {
            return if (youtubeHandle.length == 24 && youtubeHandle[0] == 'U') {
                "channel/$youtubeHandle"
            } else {
                youtubeHandle
            }
        }

        fun contactMatcher(withText: String) = object :
            BoundedMatcher<OfficialContactAdapter.ViewHolder, OfficialContactAdapter.ViewHolder>(
                OfficialContactAdapter.ViewHolder::class.java
            ) {
            override fun matchesSafely(map: OfficialContactAdapter.ViewHolder): Boolean {
                val view = map.itemView as? TextView ?: return false

                return if (view.id != R.id.contactDataText) {
                    return false
                } else {
                    view.text.toString() == withText
                }
            }

            override fun describeTo(description: Description) {
                description.appendText("with item text: $withText")
            }
        }
    }
}