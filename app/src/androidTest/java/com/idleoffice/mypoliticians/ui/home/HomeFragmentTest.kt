/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home

import androidx.test.espresso.IdlingRegistry
import androidx.test.rule.ActivityTestRule
import com.idleoffice.mypoliticians.R
import com.idleoffice.mypoliticians.helpers.FakeOfficials
import com.idleoffice.mypoliticians.idlingresource.AppIdlingResource
import com.idleoffice.mypoliticians.model.favorite.FavoritesProvider
import com.idleoffice.mypoliticians.ui.base.mvi.SearchResultState
import com.idleoffice.mypoliticians.ui.home.HomeFragmentRobot.Companion.homeRobot
import com.idleoffice.mypoliticians.ui.main.MainActivity
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.get

class HomeFragmentTest : KoinTest {

    private val activityTestRule = ActivityTestRule(MainActivity::class.java, false, false)

    private lateinit var appIdlingResource: AppIdlingResource

    companion object {
        const val DEFAULT_ADDRESS = "123 Fake Street"
    }

    @Before
    fun setUp() {
        appIdlingResource = get()
        IdlingRegistry.getInstance().register(appIdlingResource)
    }

    @After
    fun cleanUp() {
        IdlingRegistry.getInstance().unregister(appIdlingResource)
    }

    @Test
    fun disallowEmptySearchAddress() {
        robot {
            doAction {
                typeAddressIntoSearch("")
                tapKeypadSearchButton()
            }

            verify {
                toastText(R.string.home_empty_search_msg)
            }
        }
    }

    @Test
    fun ensureNoResultDisplayedOnFirstLaunch() {
        robot {
            verify {
                noResultsViewIsDisplayed()
                numberOfOfficials(0)
            }
        }
    }

    @Test
    fun ensureFirstTimeSearchContent() {
        val officials = FakeOfficials.basicFakeOfficials
        val resultState = SearchResultState.Content(officials)

        robot(null, listOf(resultState)) {
            doAction {
                typeAddressIntoSearch("11111")
                tapKeypadSearchButton()
            }

            verify {
                noResultsViewNotDisplayed()
                officialInList(officials[0])
                officialInList(officials[1])
            }
        }

    }

    @Test
    fun validationErrorOnResult() {
        val resultState = SearchResultState.ValidationError(listOf("test"))
        robot(null, listOf(resultState)) {
            doAction {
                typeAddressIntoSearch(DEFAULT_ADDRESS)
                tapKeypadSearchButton()
            }

            verify {
                toastText(R.string.validation_error)
            }
        }
    }

    @Test
    fun nullBodyOnSearchResult() {
        val resultState = SearchResultState.NullBodyError
        robot(null, listOf(resultState)) {
            doAction {
                typeAddressIntoSearch(DEFAULT_ADDRESS)
                tapKeypadSearchButton()
            }

            verify {
                toastText(R.string.validation_error)
            }
        }
    }

    @Test
    fun networkErrorOnResult() {
        val resultState = SearchResultState.NetworkError(403, "Unauthorized test")

        robot(null, listOf(resultState)) {
            doAction {
                typeAddressIntoSearch(DEFAULT_ADDRESS)
                tapKeypadSearchButton()
            }

            verify {
                toastText(R.string.search_result_error)

            }

        }
    }

    @Test
    fun noNetworkFound() {
        val resultState = SearchResultState.CallFailure(Exception("Test"))

        robot(null, listOf(resultState)) {
            doAction {
                typeAddressIntoSearch(DEFAULT_ADDRESS)
                tapKeypadSearchButton()
            }

            verify {
                toastText(R.string.no_network_found)

            }

        }
    }

    @Test
    fun lastAddressDisplayed() {
        val resultState = SearchResultState.Content(mutableListOf())

        robot(DEFAULT_ADDRESS, listOf(resultState)) {
            verify {
                lastAddressMatches(DEFAULT_ADDRESS)

            }
        }
    }

    @Test
    fun checkLastAddressChangedOnSearch() {
        val resultState = SearchResultState.Content(mutableListOf())
        val expectedNewAddress = "321 Fake Street 12345"

        robot(DEFAULT_ADDRESS, listOf(resultState)) {
            doAction {
                typeAddressIntoSearch(expectedNewAddress)
                tapKeypadSearchButton()
            }

            verify {
                lastAddressMatches(expectedNewAddress)
            }
        }
    }

    @Test
    fun officialFavoritesUpdated() {
        val officials = FakeOfficials.basicFakeOfficials
        val resultState = SearchResultState.Content(officials)

        val favoriteOfficial = officials[0].copy(isFavorite = true)

        robot(DEFAULT_ADDRESS, listOf(resultState)) {
            // Add a favorite after start from the background
            val favoritesProvider: FavoritesProvider = get()
            runBlocking {
                favoritesProvider.addFavoriteOfficial(officials[0])
            }

            verify {
                officialInList(favoriteOfficial)
            }
        }
    }

    @Test
    fun previousSearchPreferenceWithSuccessResult() {
        val resultState = SearchResultState.Content(FakeOfficials.basicFakeOfficials)

        robot(DEFAULT_ADDRESS, listOf(resultState)) {
            verify {
                numberOfOfficials(2)
                officialAtPositionMatches(0, FakeOfficials.basicFakeOfficials[0])
                officialAtPositionMatches(1, FakeOfficials.basicFakeOfficials[1])
            }
        }
    }

    @Test
    fun longListOfOfficialsOnStart() {
        val fakeOfficials = FakeOfficials.generateFakeOfficials(50)
        val resultState = SearchResultState.Content(fakeOfficials)

        robot(DEFAULT_ADDRESS, listOf(resultState)) {
            verify {
                numberOfOfficials(fakeOfficials.size)
                officialInList(fakeOfficials.last())
            }
        }
    }

    @Test
    fun listSurvivesLifecycleChange() {
        val fakeOfficials = FakeOfficials.basicFakeOfficials
        val resultState = SearchResultState.Content(fakeOfficials)

        robot(DEFAULT_ADDRESS, listOf(resultState)) {
            activityTestRule.runOnUiThread {
                activityTestRule.activity.recreate()
            }
            verify {
                numberOfOfficials(fakeOfficials.size)
            }
        }
    }

    @Test
    fun newSearchForcesScrollToTop() {
        val targetOfficial = 42
        val numberOfOfficials = targetOfficial + 50

        val fakeOfficials = FakeOfficials.generateFakeOfficials(numberOfOfficials)
        val resultState = SearchResultState.Content(fakeOfficials)
        val secondResultState =
            SearchResultState.Content(fakeOfficials.subList(0, numberOfOfficials - 2))

        robot(DEFAULT_ADDRESS, listOf(resultState, secondResultState)) {
            doAction {
                scrollToHolderPosition(targetOfficial)
                typeAddressIntoSearch("12345")
                tapKeypadSearchButton()
            }

            verify {
                officialIsVisible(fakeOfficials[0])
            }
        }
    }

    @Test
    fun configChangeRetainsPosition() {
        val targetOfficial = 42
        val numberOfOfficials = targetOfficial + 50

        val fakeOfficials = FakeOfficials.generateFakeOfficials(numberOfOfficials)
        val resultState = SearchResultState.Content(fakeOfficials)

        robot(DEFAULT_ADDRESS, listOf(resultState)) {
            doAction {
                scrollToHolderPosition(targetOfficial)
                activityTestRule.runOnUiThread {
                    activityTestRule.activity.recreate()
                }
            }

            verify {
                officialIsVisible(fakeOfficials[targetOfficial])
            }
        }
    }

    private fun robot(
        lastAddress: String? = null,
        resultState: List<SearchResultState>? = null,
        func: HomeFragmentRobot.() -> Unit
    ): HomeFragmentRobot {
        HomeFragmentSetup(this).setup(
            lastAddress,
            resultState,
            activityTestRule
        )

        return homeRobot(func)
    }
}