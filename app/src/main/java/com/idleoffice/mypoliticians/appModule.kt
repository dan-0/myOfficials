/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians

import com.idleoffice.mypoliticians.model.db.FavoriteResolver
import com.idleoffice.mypoliticians.model.wrappers.LocalBroadcastManagerWrapper
import com.idleoffice.mypoliticians.model.wrappers.LocalBroadcastManagerWrapperImpl
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val appModule = module {
    single { LocalBroadcastManagerWrapperImpl(androidApplication()) as LocalBroadcastManagerWrapper }
    single(createdAtStart = true) { FavoriteResolver(get(), get()) }
    factory { androidApplication().resources }
}