/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.retrofit

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Call
import okhttp3.EventListener
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import java.io.IOException
import java.util.concurrent.TimeUnit

val civicRequestModule = module {
    factory {
        Moshi.Builder()
            .add(CivicJsonAdapter(get()))
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    factory {
        val timeout = 10L
        OkHttpClient.Builder()
            .connectTimeout(timeout, TimeUnit.SECONDS)
            .writeTimeout(timeout, TimeUnit.SECONDS)
            .eventListenerFactory {
                return@eventListenerFactory object : EventListener() {
                    init {
                        Timber.d("Request: ${it.request().url()}")
                    }

                    override fun callFailed(call: Call, ioe: IOException) {
                        super.callFailed(call, ioe)
                        Timber.d("Request: ${call.request().url()}")
                    }

                    override fun requestBodyEnd(call: Call, byteCount: Long) {
                        Timber.d("Request: ${call.request().url()}")
                        super.requestBodyEnd(call, byteCount)
                    }
                }
            }
            .build()
    }

    single {
        Retrofit.Builder()
            .baseUrl("https://www.googleapis.com/")
            .addConverterFactory(MoshiConverterFactory.create(get()).asLenient())
            .client(get())
            .build()
            .create(CivicRequestService::class.java)
    }
}