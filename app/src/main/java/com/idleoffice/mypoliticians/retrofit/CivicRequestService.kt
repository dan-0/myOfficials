/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.retrofit

import com.idleoffice.mypoliticians.BuildConfig
import com.idleoffice.mypoliticians.ui.base.mvi.SearchResultState
import retrofit2.http.GET
import retrofit2.http.Query

interface CivicRequestService {
    @GET("/civicinfo/v2/representatives?key=${BuildConfig.CIVIC_API_KEY}&fields=offices,officials")
    suspend fun getRepresentativesAsync(@Query("address") address: String): SearchResultState
}