/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import com.idleoffice.mypoliticians.ui.main.MainActivity
import me.danlowe.idleconsent.IdleConsent
import me.danlowe.idleconsent.IdleConsentCallback
import me.danlowe.idleconsent.IdleConsentConfig
import me.danlowe.idleconsent.IdleInfoSource

class ConsentActivity : AppCompatActivity() {

    private val idleConfig by lazy {
        IdleConsentConfig(
            consentTitle = getString(R.string.disclosures),
            introStatement = getString(R.string.consent_intro_statement),
            dataCollectedSummary = getString(R.string.consent_data_collected_summary),
            dataCollected = listOf(
                getString(R.string.consent_data_collected_dev_info),
                getString(R.string.consent_data_collected_usage),
                getString(R.string.consent_data_collected_advertising_id)),
            privacyInfoSource = IdleInfoSource.Web(
                getString(R.string.consent_privacy_info_link_text),
                Uri.parse("https://myofficials.app/privacy/")
            ),
            requirePrivacy = true,
            acceptPrivacyPrompt = getString(R.string.consent_accept_privacy_prompt),
            privacyPromptChecked = true,
            termsSummary = getString(R.string.consent_terms_summary),
            termsInfoSource = IdleInfoSource.Web(
                getString(R.string.consent_terms_info_source_link_text),
                Uri.parse("https://myofficials.app/terms/")
            ),
            version = 1
        )
    }

    private val consentCallback = object : IdleConsentCallback() {
        override fun onAcknowledged(hasUserAgreedToTerms: Boolean, hasUserAgreedToPrivacy: Boolean) {
            if (hasUserAgreedToTerms) {
                startMainActivity()
            }
        }
    }

    private fun startMainActivity() {
        if (lifecycle.currentState == Lifecycle.State.DESTROYED) {
            return
        }
        Intent(this@ConsentActivity, MainActivity::class.java).also {
            startActivity(it)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(LinearLayout(this))
        val consent = IdleConsent(this)
        if (!consent.hasUserAgreedToTerms() || consent.isNewConsentVersion(idleConfig.version)) {
            consent.showConsentDialog(supportFragmentManager, consentCallback, idleConfig)
        } else {
            startMainActivity()
        }
    }

}