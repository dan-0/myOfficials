/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.data

import android.os.Parcelable
import androidx.annotation.DrawableRes
import com.idleoffice.mypoliticians.R
import kotlinx.android.parcel.Parcelize

data class ChannelResponse(
    val id: String?,
    val type: String?
)

@Parcelize
data class Channel(
    val id: String?,
    val type: ChannelType?
) : Parcelable

enum class ChannelType(val friendlyName: String, @DrawableRes val icon: Int) {
    TWITTER("Twitter", R.drawable.ic_twitter_social_icon_square_color),
    FACEBOOK("Facebook", R.drawable.ic_facebook_72),
    YOUTUBE("YouTube", R.drawable.ic_youtube_icon)
}

fun ChannelResponse.toChannel(): Channel? {
    val channelType = ChannelType.values().firstOrNull {
        type.equals(it.friendlyName, false)
    }

    return when {
        id == null || channelType == null -> null
        else -> Channel(id, channelType)
    }
}