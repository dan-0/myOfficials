/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.data

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.idleoffice.mypoliticians.R
import kotlinx.android.parcel.Parcelize

/**
 * @property name Official's name
 * @property office Office held by the official, eg "President"
 * @property party Official's political party
 * @property address List of physical addresses associated with the official
 * @property phones List of phone numbers associated with the official
 * @property emails List of email addresses associated with the official
 * @property urls List of websites associated with the official
 * @property twitterChannel Twitter channel associated with the official
 * @property facebookChannel Facebook channel associated with the official
 * @property youtubeChannel Youtube channel associated with the official
 * @property photoUrl URL for image of official
 * @property isFavorite true if the official is favorited by the user
 */
@Entity(tableName = "officials")
@Parcelize
data class Official(
    @PrimaryKey
    val id: Int,
    val name: String,
    val office: String,
    val party: String?,
    val address: List<OfficialContactData>?,
    val phones: List<OfficialContactData>?,
    val emails: List<OfficialContactData>?,
    val urls: List<OfficialContactData>?,
    val twitterChannel: Channel?,
    val facebookChannel: Channel?,
    val youtubeChannel: Channel?,
    val photoUrl: String?,
    val isFavorite: Boolean = false
) : Parcelable

@Parcelize
data class OfficialContactData(
    val type: ContactType,
    val value: String
) : Parcelable

enum class ContactType(val friendlyName: String, @DrawableRes val icon: Int) {
    ADDRESS("address", R.drawable.ic_place_tertiary_36dp),
    PHONE("phone", R.drawable.ic_phone_tertiary_36dp),
    URL("url", R.drawable.ic_link_tertiary_36dp),
    EMAIL("email", R.drawable.ic_email_tertiary_36dp)
}