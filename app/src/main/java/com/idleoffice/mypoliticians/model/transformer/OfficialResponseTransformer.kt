/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.transformer

import android.content.res.Resources
import com.idleoffice.mypoliticians.R
import com.idleoffice.mypoliticians.model.data.*
import com.idleoffice.mypoliticians.ui.base.mvi.SearchResultState
import java.util.*


fun OfficialsResponse.transform(resources: Resources): SearchResultState {
    val officials: MutableList<Official> = mutableListOf()

    offices?.forEach officeEach@{ office ->
        office.officialIndices?.forEach indexEach@{ i ->
            val numOfficials = officials.size
            if (numOfficials < i - 1) {
                return SearchResultState.ValidationError(listOf("office index $i greater than number of officials $numOfficials"))
            }

            val responseOfficial = if (officialResponses == null) {
                return SearchResultState.EmptyOfficialsError
            } else {
                officialResponses[i]
            }

            if (responseOfficial.name == null || office.name == null) {
                return@indexEach
            }

            val addresses = responseOfficial.address?.mapNotNull { addressResponse ->
                addressResponse.toFriendlyAddress()
            }

            val channels = responseOfficial.channels?.mapNotNull { channelResponse ->
                channelResponse.toChannel()
            }

            val uniqueId = (responseOfficial.name + office.name).hashCode()

            val party = when (responseOfficial.party?.toLowerCase(Locale.ROOT)) {
                null,
                "",
                "unknown" -> resources.getString(R.string.political_party_unknown)
                else -> responseOfficial.party
            }

            officials.add(
                Official(
                    id = uniqueId,
                    name = responseOfficial.name,
                    office = office.name,
                    party = party,
                    address = addresses?.map { OfficialContactData(ContactType.ADDRESS, it) },
                    phones = responseOfficial.phones?.map {
                        OfficialContactData(
                            ContactType.PHONE,
                            it
                        )
                    },
                    emails = responseOfficial.emails?.map {
                        OfficialContactData(
                            ContactType.EMAIL,
                            it
                        )
                    },
                    urls = responseOfficial.urls?.map { OfficialContactData(ContactType.URL, it) },
                    twitterChannel = channels?.firstOrNull { it.type == ChannelType.TWITTER },
                    facebookChannel = channels?.firstOrNull { it.type == ChannelType.FACEBOOK },
                    youtubeChannel = channels?.firstOrNull { it.type == ChannelType.YOUTUBE },
                    photoUrl = responseOfficial.photoUrl

                )
            )
        }
    }

    return SearchResultState.Content(officials)
}

