/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.favorite

import com.idleoffice.mypoliticians.idlingresource.AppIdlingResource
import com.idleoffice.mypoliticians.model.dao.FavoriteDao
import com.idleoffice.mypoliticians.model.data.Official
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FavoritesProviderImpl(
    private val favoritesDao: FavoriteDao,
    private val idlingResource: AppIdlingResource,
    ioScope: CoroutineScope = CoroutineScope(Dispatchers.IO)
) : FavoritesProvider {

    private val _favoritesChannel = BroadcastChannel<List<Official>>(Channel.CONFLATED)

    override val favoritesChannel: Flow<List<Official>>
        get() = _favoritesChannel.asFlow()

    init {
        ioScope.launch {
            idlingResource.start()
            updateChannel()
        }
    }

    override suspend fun addFavoriteOfficial(official: Official) {
        withContext(Dispatchers.IO) {
            idlingResource.start()
            favoritesDao.insertOfficial(official)
            updateChannel()
        }
    }

    override suspend fun removeFavoriteOfficial(official: Official) {
        withContext(Dispatchers.IO) {
            idlingResource.start()
            favoritesDao.removeOfficial(official)
            updateChannel()
        }
    }

    private suspend fun updateChannel() {
        val favorites = favoritesDao.getAll()
        _favoritesChannel.send(favorites)
        idlingResource.stop()
    }
}