/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.db

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.idleoffice.mypoliticians.model.coroutines.getNewIoScope
import com.idleoffice.mypoliticians.model.dao.FavoriteDao
import com.idleoffice.mypoliticians.model.data.Official
import com.idleoffice.mypoliticians.model.wrappers.LocalBroadcastManagerWrapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

/**
 * Listens for a broadcast indicating a favorite preference has changed and resolves the difference with the
 * open databases
 */
class FavoriteResolver(
    favoriteDao: FavoriteDao,
    broadcastManager: LocalBroadcastManagerWrapper,
    favoriteChangeReceiver: FavoriteChangeReceiver = FavoriteChangeReceiver(favoriteDao)
) {
    companion object {
        const val OFFICIAL_FAVORITE_KEY = "official_id_changed"
        const val OFFICIAL_FAVORITE_INTENT = "official_favorite_changed"
    }

    init {
        broadcastManager.registerReceiver(
            favoriteChangeReceiver,
            IntentFilter(OFFICIAL_FAVORITE_INTENT)
        )
    }

    class FavoriteChangeReceiver(
        private val favoriteDao: FavoriteDao,
        private val ioScope: CoroutineScope = getNewIoScope()
    ) : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            intent ?: return

            val official: Official = intent.getParcelableExtra(OFFICIAL_FAVORITE_KEY)!!

            ioScope.launch {
                if (official.isFavorite) {
                    favoriteDao.insertOfficial(official)
                } else {
                    favoriteDao.removeOfficial(official)
                }
            }
        }
    }
}