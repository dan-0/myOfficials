/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.data


data class AddressResponse(
    val city: String?,
    val line1: String?,
    val line2: String?,
    val line3: String?,
    val locationName: String?,
    val state: String?,
    val zip: String?
)

fun AddressResponse.toFriendlyAddress(): String? {
    val sb = StringBuilder()

    locationName?.appendToBuilder(sb)
    line1?.appendToBuilder(sb)
    line2?.appendToBuilder(sb)
    line3?.appendToBuilder(sb)
    city?.appendToBuilder(sb)
    state?.appendToBuilder(sb)
    zip?.appendToBuilder(sb)

    val address = sb.toString().substringBeforeLast(", ")

    return if (address.isEmpty()) null else address
}

private fun String.appendToBuilder(sb: StringBuilder) {
    if (this.isNotEmpty()) sb.append("$this, ")
}