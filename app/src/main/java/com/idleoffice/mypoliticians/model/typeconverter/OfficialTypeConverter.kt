/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.typeconverter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.idleoffice.mypoliticians.model.data.Channel
import com.idleoffice.mypoliticians.model.data.OfficialContactData

/**
 * Converter to help store an Official as a database entity
 */
class OfficialTypeConverter(
    private val gson: Gson = Gson()
) {
    @TypeConverter
    fun listContactDataToString(data: List<OfficialContactData>?): String? {
        data ?: return null

        return gson.toJson(data)
    }

    @TypeConverter
    fun stringToContactData(data: String?): List<OfficialContactData>? {
        data ?: return null
        val type = object : TypeToken<List<OfficialContactData>?>() {}.type
        return gson.fromJson(data, type)
    }

    @TypeConverter
    fun channelToString(data: Channel?): String? {
        data ?: return null
        return gson.toJson(data)
    }

    @TypeConverter
    fun stringToChannel(data: String?): Channel? {
        data ?: return null

        return gson.fromJson(data, Channel::class.java)
    }
}