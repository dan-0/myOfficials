/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.data

import com.squareup.moshi.Json

data class OfficialsResponse(
    val offices: List<OfficeResponse>?,
    @Json(name = "officials") val officialResponses: List<OfficialResponse>?
)

/**
 * @property name Name of the office being represented, eg Vice President
 * @property officialIndices Index of the [OfficialResponse]s associated with this office
 */
data class OfficeResponse(
    val name: String?,
    @Suppress("ArrayInDataClass") val officialIndices: IntArray?
)

data class OfficialResponse(
    val address: List<AddressResponse>?,
    val channels: List<ChannelResponse>?,
    val emails: List<String>?,
    val name: String?,
    val party: String?,
    val phones: List<String>?,
    val photoUrl: String?,
    val urls: List<String>?
)