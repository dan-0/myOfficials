/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians

import android.app.Application
import com.idleoffice.mypoliticians.analytics.analyticModule
import com.idleoffice.mypoliticians.idlingresource.idlingResourceModule
import com.idleoffice.mypoliticians.logging.TimberTree
import com.idleoffice.mypoliticians.model.db.dbModule
import com.idleoffice.mypoliticians.model.favorite.favoritesProviderModule
import com.idleoffice.mypoliticians.model.log.KoinTimberLogger
import com.idleoffice.mypoliticians.model.sharedprefs.sharedPrefsModule
import com.idleoffice.mypoliticians.retrofit.civicRequestModule
import com.idleoffice.mypoliticians.ui.favorite.favoriteModule
import com.idleoffice.mypoliticians.ui.home.homeModule
import com.idleoffice.mypoliticians.ui.official.officialActivityModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class MainApp : Application() {

    private val koinModules = listOf(
        civicRequestModule,
        analyticModule,
        officialActivityModule,
        homeModule,
        sharedPrefsModule,
        dbModule,
        favoriteModule,
        appModule,
        favoritesProviderModule,
        idlingResourceModule
    )

    override fun onCreate() {

        Timber.plant(TimberTree())

        startKoin {
            KoinTimberLogger()
            androidContext(this@MainApp)
            modules(koinModules)
        }

        super.onCreate()
    }
}