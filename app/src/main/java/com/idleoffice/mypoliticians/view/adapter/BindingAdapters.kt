/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.view.adapter

import android.content.Context
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.databinding.BindingAdapter
import com.idleoffice.mypoliticians.R

object BindingAdapters {
    @BindingAdapter("onEditorActionListener")
    @JvmStatic
    fun onEditorActionListener(view: EditText, action: (String?) -> Unit) {
        if (view.id != R.id.addressText) {
            throw IllegalStateException("Invalid use of `hideKeyboardOnSearch`, must be on `addressText`")
        }

        view.setOnEditorActionListener {  _, actionId, _ ->
            when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    action(view.text.toString())
                    view.clearTextInput()
                    true }
                else -> false
            }
        }
    }

    private fun EditText.clearTextInput() {
        clearFocus()
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)?.also {
            it.hideSoftInputFromWindow(windowToken, 0)
        }
    }
}