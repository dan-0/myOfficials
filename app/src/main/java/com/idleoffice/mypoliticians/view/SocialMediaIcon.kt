/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatImageButton
import com.idleoffice.mypoliticians.R
import com.idleoffice.mypoliticians.model.data.Channel
import com.idleoffice.mypoliticians.model.data.Official

class SocialMediaIcon @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageButton(context, attrs, defStyleAttr) {

    var official: Official? = null
    var channel: Channel? = null

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        channel?.apply {
            official?.let {
                contentDescription = generateContentDescription(it, this)
            }

            type?.icon?.let { setImageResource(it) }

        } ?: run {
            visibility = View.GONE
        }
    }

    private fun generateContentDescription(official: Official, channel: Channel): String {
        val type = channel.type
        val office = official.office
        val name = official.name

        return context.getString(R.string.social_media_cd, type, office, name)
    }
}