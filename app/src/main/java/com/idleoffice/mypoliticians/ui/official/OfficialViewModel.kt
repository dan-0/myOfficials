/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.official

import androidx.lifecycle.ViewModel
import com.idleoffice.mypoliticians.analytics.AnalyticService
import com.idleoffice.mypoliticians.idlingresource.AppIdlingResource
import com.idleoffice.mypoliticians.model.data.Official
import com.idleoffice.mypoliticians.model.favorite.FavoritesProvider
import com.idleoffice.mypoliticians.ui.official.mvi.OfficialAction
import com.idleoffice.mypoliticians.ui.official.mvi.OfficialState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.consumeAsFlow

class OfficialViewModel(
    private val analyticService: AnalyticService,
    private val favoritesProvider: FavoritesProvider,
    private val idlingResource: AppIdlingResource,
    initialOfficial: Official
) : ViewModel() {

    private val _officialEvent = Channel<OfficialState>(Channel.CONFLATED)
    val officialEvent: Flow<OfficialState> = _officialEvent.consumeAsFlow()

    private val _officialState =
        ConflatedBroadcastChannel<OfficialState.Content>(OfficialState.Init(initialOfficial))
    val officialState: Flow<OfficialState.Content> = _officialState.asFlow()

    suspend fun takeAction(action: OfficialAction) {
        idlingResource.start()
        analyticService.newAction(action)

        val newState = when (action) {
            is OfficialAction.SocialMediaClicked -> OfficialState.OpenSocialMedia(action.channel)
            is OfficialAction.ContactInfoClicked -> OfficialState.OpenContact(action.contactData)
            OfficialAction.FavoriteClicked -> handleFavoriteUpdate()
        }

        updateState(newState)

        idlingResource.stop()
    }

    private suspend fun handleFavoriteUpdate(): OfficialState {
        val newState = _officialState.value.let {
            it.official.copy(isFavorite = !it.official.isFavorite)
        }

        newState.run {
            if (isFavorite) {
                favoritesProvider.addFavoriteOfficial(this)
            } else {
                favoritesProvider.removeFavoriteOfficial(this)
            }
        }

        return OfficialState.UpdateFavorite(newState)
    }

    private suspend fun updateState(newState: OfficialState) {
        analyticService.newEvent(newState)
        when (newState) {
            is OfficialState.Content -> _officialState.send(newState)
            is OfficialState.Event -> _officialEvent.send(newState)
        }
    }
}