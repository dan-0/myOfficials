/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.idleoffice.mypoliticians.R
import com.idleoffice.mypoliticians.databinding.FragmentFavoritesBinding
import com.idleoffice.mypoliticians.model.data.Official
import com.idleoffice.mypoliticians.ui.base.MviFragment
import com.idleoffice.mypoliticians.ui.favorite.mvi.FavoriteActor
import com.idleoffice.mypoliticians.ui.favorite.mvi.FavoriteViewState
import com.idleoffice.mypoliticians.ui.favorite.view.FavoritesNoResultsText
import com.idleoffice.mypoliticians.ui.home.HomeFragment
import com.idleoffice.mypoliticians.ui.home.OfficialAdapter
import kotlinx.android.synthetic.main.fragment_favorites.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.core.qualifier.named

class FavoriteFragment : MviFragment() {
    private val favoriteViewModel: FavoriteViewModel by sharedViewModel()

    private val koinScope = getKoin().getOrCreateScope("favorite", named<HomeFragment>())
    private val officialAdapter: OfficialAdapter by koinScope.inject()

    private lateinit var binding: FragmentFavoritesBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentFavoritesBinding.inflate(inflater, container, false)
        binding.actor = FavoriteActor(mainScope, favoriteViewModel::takeAction)
        binding.notifyChange()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView(officialAdapter)
        startObservers()
    }

    private fun startObservers() {
        ioScope.launch {
            favoriteViewModel.favoriteState.collect {  state ->
                withContext(Dispatchers.Main) {
                    when (state) {
                        is FavoriteViewState.Content -> updateData(state.data)
                    }
                }
            }
        }
    }

    private fun updateData(data: List<Official>) {
        officialAdapter.officials.clear()
        officialAdapter.officials.addAll(data)
        officialAdapter.notifyDataSetChanged()

        view?.findViewById<FavoritesNoResultsText>(R.id.noResultsText)?.run {
            visibility = if (data.isEmpty()) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }


    private fun initRecyclerView(officialAdapter: OfficialAdapter) {
        val linearLayoutManager = LinearLayoutManager(context)
        officialsList.apply {
            itemAnimator = DefaultItemAnimator()
            adapter = officialAdapter

            layoutManager = linearLayoutManager
        }
    }
}