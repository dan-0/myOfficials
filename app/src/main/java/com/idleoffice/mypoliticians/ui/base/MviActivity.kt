/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.idleoffice.mypoliticians.model.coroutines.getNewDefaultScope
import com.idleoffice.mypoliticians.model.coroutines.getNewIoScope
import com.idleoffice.mypoliticians.model.coroutines.getNewMainScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob

abstract class MviActivity : AppCompatActivity() {
    private lateinit var job: Job
    protected lateinit var ioScope: CoroutineScope
    protected lateinit var mainScope: CoroutineScope
    protected lateinit var defaultScope: CoroutineScope

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = SupervisorJob()
        ioScope = getNewIoScope(job)
        mainScope = getNewMainScope(job)
        defaultScope = getNewDefaultScope(job)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!job.isCancelled) {
            job.cancel()
        }
    }
}