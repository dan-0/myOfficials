/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.main

import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.idleoffice.mypoliticians.R
import com.idleoffice.mypoliticians.ui.favorite.FavoriteFragment
import com.idleoffice.mypoliticians.ui.home.HomeFragment
import kotlinx.coroutines.*
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    private var currentActionView: Int = 0

    companion object {
        private const val CURRENT_ACTION_KEY = "current_action_view"
    }

    private val homeFragment by lazy { HomeFragment() }
    private val favoriteFragment by lazy { FavoriteFragment() }

    private lateinit var mainJob: Job
    private lateinit var mainScope: CoroutineScope

    private var backButtonCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainJob = Job()
        mainScope = CoroutineScope(mainJob + Dispatchers.Main)

        findViewById<BottomNavigationView>(R.id.appBottomNav).setOnNavigationItemSelectedListener {
            loadAction(it)
        }

        loadLastActionFromBundle(savedInstanceState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onRestoreInstanceState(savedInstanceState, persistentState)
        loadLastActionFromBundle(savedInstanceState)
    }

    private fun loadAction(menuItem: MenuItem): Boolean {
        val action = menuItem.itemId
        val index = getMenuIndex(action)
        if (currentActionView == action) {
            return true
        }

        val fragment = getFragmentFromAction(action)

        return if (fragment != null) {
            val previousIndex = getMenuIndex(findViewById<BottomNavigationView>(R.id.appBottomNav).selectedItemId)
            val animators = if (index > previousIndex) {
                Pair(R.animator.enter_from_right, R.animator.exit_to_left)
            } else {
                Pair(R.animator.enter_from_left, R.animator.exit_to_right)
            }

            Timber.d("Replacing fragment view with fragment ${fragment::class.simpleName}")

            supportFragmentManager.beginTransaction()
                .setCustomAnimations(animators.first, animators.second)
                .replace(R.id.view_content, fragment)
                .addToBackStack(null)
                .commit()

            currentActionView = action
            true
        } else {
            Timber.e("Invalid action attempted to be loaded.")
            false
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(CURRENT_ACTION_KEY, currentActionView)
        super.onSaveInstanceState(outState)
    }

    private fun loadLastActionFromBundle(bundle: Bundle?) {
        val lastAction = bundle?.getInt(CURRENT_ACTION_KEY) ?: 0

        val nextAction = if (lastAction != 0) {
            lastAction
        } else {
            R.id.action_home
        }

        val fragment = getFragmentFromAction(nextAction) ?: return

        val fragTag = fragment::class.java.name
        val fragExists = supportFragmentManager.findFragmentByTag(fragTag) != null

        if (!fragExists) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.view_content, fragment, fragment::class.java.name)
                .commit()
        }
    }

    private fun getFragmentFromAction(action: Int): Fragment? = when (action) {
        R.id.action_home -> {
            homeFragment
        }
        R.id.action_favorite -> {
            favoriteFragment
        }
        else -> null
    }

    private fun getMenuIndex(@IdRes itemId: Int): Int = when (itemId) {
        R.id.action_home -> 0
        R.id.action_favorite -> 1
        else -> -1
    }

    override fun onBackPressed() {

        when (backButtonCount) {
            0 -> {
                backButtonCount++
                Toast.makeText(this, getString(R.string.back_to_exit), Toast.LENGTH_SHORT).show()

                mainScope.launch {
                    delay(3000)
                    backButtonCount = 0
                }
            }
            else -> finish()
        }
    }

    override fun onDestroy() {
        if (!mainJob.isCancelled) {
            mainJob.cancel()
        }
        super.onDestroy()
    }
}
