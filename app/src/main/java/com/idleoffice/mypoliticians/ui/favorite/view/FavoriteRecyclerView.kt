/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.favorite.view

import android.content.Context
import android.util.AttributeSet
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.idleoffice.mypoliticians.ui.favorite.FavoriteSwipeToDeleteCallback
import com.idleoffice.mypoliticians.ui.favorite.mvi.FavoriteActor
import com.idleoffice.mypoliticians.ui.home.OfficialAdapter

class FavoriteRecyclerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {
    companion object {
        @BindingAdapter("swipeDeleteHandler", requireAll = false)
        @JvmStatic
        fun swipeDeleteHandler(recyclerView: FavoriteRecyclerView, actor: FavoriteActor) {
            recyclerView.adapter?.let {
                if (it !is OfficialAdapter) {
                    return
                }

                val swipeCallback = FavoriteSwipeToDeleteCallback(it, actor::handleSwipe)
                ItemTouchHelper(swipeCallback).attachToRecyclerView(recyclerView)
            }
        }
    }
}