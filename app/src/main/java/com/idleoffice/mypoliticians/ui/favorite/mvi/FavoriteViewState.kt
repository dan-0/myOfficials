/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.favorite.mvi

import com.idleoffice.mypoliticians.analytics.AppEvent
import com.idleoffice.mypoliticians.model.data.Official

sealed class FavoriteViewState(event: String): AppEvent(event, "favorite_state") {
    sealed class Content(val data: List<Official>, event: String) : FavoriteViewState(event) {
        class Init(data: List<Official> = listOf()) : Content(data, "official_state_official_init")
        class UpdateFavorite(data: List<Official>) : Content(data, "official_state_official_update")
    }
}