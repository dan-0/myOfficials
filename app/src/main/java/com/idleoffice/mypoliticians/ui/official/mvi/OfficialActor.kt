/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.official.mvi

import com.idleoffice.mypoliticians.model.data.Channel
import com.idleoffice.mypoliticians.model.data.OfficialContactData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class OfficialActor(
    private val scope: CoroutineScope,
    private val take: suspend (OfficialAction) -> Unit
) : OfficialContactActor {

    override fun clickContact(contactData: OfficialContactData) {
        scope.launch {
            take(OfficialAction.ContactInfoClicked(contactData))
        }
    }

    fun clickSocialMedia(channel: Channel) {
        scope.launch {
            // Be explicit with the NPE here, null shouldn't be  a real case
            take(OfficialAction.SocialMediaClicked(channel))
        }
    }

    fun clickFavorite() {
        scope.launch {
            take(OfficialAction.FavoriteClicked)
        }
    }
}