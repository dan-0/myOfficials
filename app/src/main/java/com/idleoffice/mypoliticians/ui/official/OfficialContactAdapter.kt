/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.official

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.idleoffice.mypoliticians.databinding.RowOfficialContactBinding
import com.idleoffice.mypoliticians.model.data.OfficialContactData
import com.idleoffice.mypoliticians.ui.base.BaseViewHolder
import com.idleoffice.mypoliticians.ui.official.mvi.OfficialContactActor

class OfficialContactAdapter(
    val officialContacts: List<OfficialContactData>,
    val actor: OfficialContactActor
) : RecyclerView.Adapter<BaseViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowOfficialContactBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = officialContacts.size

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    inner class ViewHolder(private val binding: RowOfficialContactBinding) : BaseViewHolder(binding.root) {
        override fun onBind(position: Int) {
            binding.contactData = officialContacts[position]
            binding.actor = actor
            binding.executePendingBindings()
        }
    }
}