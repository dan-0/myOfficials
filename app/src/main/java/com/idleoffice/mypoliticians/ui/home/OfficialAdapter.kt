/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.idleoffice.mypoliticians.analytics.AnalyticService
import com.idleoffice.mypoliticians.analytics.AppAction
import com.idleoffice.mypoliticians.databinding.CardOfficialBinding
import com.idleoffice.mypoliticians.model.data.Official
import com.idleoffice.mypoliticians.model.db.FavoriteResolver.Companion.OFFICIAL_FAVORITE_KEY
import com.idleoffice.mypoliticians.ui.base.BaseViewHolder
import com.idleoffice.mypoliticians.ui.official.OfficialActivity

class OfficialAdapter(
    val officials: MutableList<Official>,
    private val analyticService: AnalyticService
) : RecyclerView.Adapter<BaseViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding = CardOfficialBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, officials, analyticService)
    }

    override fun getItemCount(): Int {
        return officials.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    /**
     * Detects changes to favorites in officials
     */
    class FavoriteChangedBroadcastReceiver(
        private val officials: MutableList<Official>,
        private val adapter: RecyclerView.Adapter<BaseViewHolder>
    ) : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            intent ?: return

            val official: Official = intent.getParcelableExtra(OFFICIAL_FAVORITE_KEY)

            val changedOfficialIdx = officials.indexOfFirst { it.id == official.id }
            if (changedOfficialIdx == -1) return

            officials[changedOfficialIdx] = official

            adapter.notifyDataSetChanged()// .notifyItemChanged(changedOfficialIdx)
        }
    }

    class ViewHolder(
        private val binding: CardOfficialBinding,
        private val officials: MutableList<Official>,
        private val analyticService: AnalyticService
    ) : BaseViewHolder(binding.root) {
        private val actor = OfficialClickedActor { takeOfficialClicked(it) }

        override fun onBind(position: Int) {
            binding.official = officials[position]
            binding.clickActor = actor
            binding.executePendingBindings()
        }

        /**
         * Handle the view for this official being clicked
         */
        private fun takeOfficialClicked(action: OfficialOverviewAction) {
            analyticService.newAction(action)

            when (action) {
                is OfficialOverviewAction.Clicked -> {
                    val context = binding.root.context
                    Intent(context, OfficialActivity::class.java).also {
                        it.putExtra(OfficialActivity.OFFICIAL_DATA, action.official)
                        context.startActivity(it)
                    }
                }
            }
        }
    }
}

class OfficialClickedActor(private val takeOfficial: (OfficialOverviewAction) -> Unit) {
    fun officialClicked(official: Official) {
        takeOfficial.invoke(OfficialOverviewAction.Clicked(official))
    }
}

sealed class OfficialOverviewAction(action: String) : AppAction(action, "official") {
    class Clicked(val official: Official) : OfficialOverviewAction("clicked")
}