/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.favorite

import android.graphics.*
import android.graphics.drawable.ColorDrawable
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.idleoffice.mypoliticians.model.data.Official
import com.idleoffice.mypoliticians.ui.home.OfficialAdapter
import kotlin.math.absoluteValue


class FavoriteSwipeToDeleteCallback(
    private val adapter: OfficialAdapter,
    private val removeHandler: (Official) -> Unit
) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

    private val clearPaint = Paint().apply {
        xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
    }

    private val background = ColorDrawable()

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean = false

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val removeIdx = viewHolder.adapterPosition
        val official = adapter.officials.removeAt(removeIdx)
        adapter.notifyItemRemoved(removeIdx)
        removeHandler(official)
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        val resources = recyclerView.context.resources
        val itemView = viewHolder.itemView

        val trashBinIcon =
            resources.getDrawable(com.idleoffice.mypoliticians.R.drawable.ic_delete_sweep_black_24dp, null)
        trashBinIcon.setTint(Color.WHITE)

        val isCancelled = dX == 0f && !isCurrentlyActive

        if (isCancelled) {
            clearCanvas(
                c,
                itemView.right + dX,
                itemView.top.toFloat(),
                itemView.right.toFloat(),
                itemView.bottom.toFloat()
            )
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            return
        }

        background.apply {
            setBounds(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)
            draw(c)
            color = if (dX.absoluteValue < viewHolder.itemView.width / 3) {
                Color.GRAY
            } else {
                Color.RED
            }
        }

        val itemHeight = itemView.height
        val deleteIconTop = itemView.top + (itemHeight - trashBinIcon.intrinsicHeight) / 2
        val deleteIconMargin = (itemHeight - trashBinIcon.intrinsicHeight) / 2
        val deleteIconLeft = itemView.right - deleteIconMargin - trashBinIcon.intrinsicWidth
        val deleteIconRight = itemView.right - deleteIconMargin
        val deleteIconBottom = deleteIconTop + trashBinIcon.intrinsicHeight

        trashBinIcon.bounds = Rect(
            deleteIconLeft,
            deleteIconTop,
            deleteIconRight,
            deleteIconBottom
        )

        trashBinIcon.draw(c)

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    private fun clearCanvas(c: Canvas, left: Float?, top: Float?, right: Float?, bottom: Float?) {
        c.drawRect(left!!, top!!, right!!, bottom!!, clearPaint)
    }
}