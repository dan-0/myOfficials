/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home

import android.content.SharedPreferences
import android.content.res.Resources
import androidx.lifecycle.ViewModel
import com.idleoffice.mypoliticians.analytics.AnalyticService
import com.idleoffice.mypoliticians.idlingresource.AppIdlingResource
import com.idleoffice.mypoliticians.model.coroutines.getNewDefaultScope
import com.idleoffice.mypoliticians.model.coroutines.getNewIoScope
import com.idleoffice.mypoliticians.model.data.Official
import com.idleoffice.mypoliticians.model.favorite.FavoritesProvider
import com.idleoffice.mypoliticians.retrofit.CivicRequestService
import com.idleoffice.mypoliticians.ui.base.mvi.SearchResultState
import com.idleoffice.mypoliticians.ui.home.mvi.HomeAction
import com.idleoffice.mypoliticians.ui.home.mvi.HomeViewState
import com.idleoffice.mypoliticians.ui.home.mvi.HomeViewStateData
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.first
import timber.log.Timber
import java.net.UnknownHostException

class HomeViewModel(
    private val civicRequestService: CivicRequestService,
    private val analyticService: AnalyticService,
    private val sharedPreferences: SharedPreferences,
    private val favoritesProvider: FavoritesProvider,
    private val resources: Resources,
    private val idlingResource: AppIdlingResource,
    private val job: Job = SupervisorJob(),
    private val ioScope: CoroutineScope = getNewIoScope(job),
    private val defaultScope: CoroutineScope = getNewDefaultScope(job)
) : ViewModel() {

    companion object {
        const val HOME_ADDRESS_KEY = "user_home_address"
    }

    private val initialState = HomeViewState.Content.Init(
        HomeViewStateData(
            lastAddress = sharedPreferences.getString(HOME_ADDRESS_KEY, "")!!
        )
    )

    private val _homeViewState = ConflatedBroadcastChannel<HomeViewState.Content>(initialState)
    val homeViewState
        get() = _homeViewState.asFlow()

    private val _homeEffect = BroadcastChannel<HomeViewState>(Channel.CONFLATED)
    fun homeEffect() = _homeEffect.openSubscription().apply {
        poll()
    }.consumeAsFlow()

    init {
        subscribeToFavoriteChanges()

        currentState().let {
            if (it.lastAddress.isNotEmpty()) {
                takeAction(HomeAction.SearchPreviousAddress(it.lastAddress))
            }
        }
    }

    /**
     * Process an instance of [HomeAction]
     */
    fun takeAction(action: HomeAction) {
        defaultScope.launch {
            idlingResource.start()
            analyticService.newAction(action)

            val newState: HomeViewState = when (action) {
                is HomeAction.SearchAddress -> searchAddress(action.searchAddress)
                is HomeAction.SearchPreviousAddress -> searchAddress(action.searchAddress)
                HomeAction.Refresh -> handleRefresh()
            }

            updateState(newState)
            idlingResource.stop()
        }
    }

    /**
     * Search a given address
     */
    private suspend fun searchAddress(searchAddress: String?): HomeViewState {
        if (searchAddress.isNullOrEmpty()) {
            return HomeViewState.Error.EmptySearchText(resources)
        }

        _homeViewState.sendBlocking(HomeViewState.Content.Loading(currentState()))

        val result = ioScope.async {
            val searchResults = civicRequestService.getRepresentativesAsync(searchAddress)
            handleSearchResults(searchResults, searchAddress)
        }

        return runCatching {
            result.await()
        }.getOrElse {
            Timber.e(it, "Unknown error contacting civic request service")
            when (it) {
                is UnknownHostException -> HomeViewState.Error.UnknownHostError(resources)
                else -> HomeViewState.Error.UnknownError(resources)
            }
        }
    }

    private suspend fun handleSearchResults(
        searchResultState: SearchResultState,
        searchAddress: String
    ): HomeViewState {
        return when (searchResultState) {
            is SearchResultState.Content -> handleValidSearchResult(
                searchResultState,
                searchAddress
            )
            is SearchResultState.NetworkError -> HomeViewState.Error.SearchResultError(resources)
            is SearchResultState.CallFailure -> HomeViewState.Error.NoNetworkError(resources)
            is SearchResultState.ValidationError,
            SearchResultState.NullBodyError -> HomeViewState.Error.ValidationError(resources)
            else -> throw IllegalArgumentException("Invalid search result data: $searchResultState")
        }
    }

    private suspend fun handleRefresh() = searchAddress(currentState().lastAddress)

    private suspend fun handleValidSearchResult(
        searchResultState: SearchResultState.Content,
        searchAddress: String
    ): HomeViewState {

        val deferredOfficials = defaultScope.async {
            val favorites = favoritesProvider.favoritesChannel.first()
            searchResultState.officials.mapFavorites(favorites)
        }

        val officials = deferredOfficials.await().toMutableList()

        val newData = currentState().copy(lastAddress = searchAddress, officials = officials)

        updateLastSearchedAddress(searchAddress)

        updateState(HomeViewState.NewResultsLoaded)
        return HomeViewState.Content.OfficialsChanged(newData)
    }

    private fun updateLastSearchedAddress(homeAddress: String) {
        sharedPreferences.edit()
            .putString(HOME_ADDRESS_KEY, homeAddress)
            .apply()
    }

    private fun subscribeToFavoriteChanges() {
        defaultScope.launch {
            favoritesProvider.favoritesChannel.collect {
                idlingResource.start()
                currentState().let { currentStateData ->
                    if (currentStateData.officials.size == 0) {
                        return@let
                    }

                    val newOfficials = currentStateData.officials.mapFavorites(it).toMutableList()
                    val newStateData = currentStateData.copy(officials = newOfficials)
                    val newState = HomeViewState.Content.FavoritesChanged(newStateData)
                    updateState(newState)
                }
                idlingResource.stop()
            }
        }
    }

    private fun List<Official>.mapFavorites(favorites: List<Official>) = map { official ->
        if (favorites.any { it.id == official.id }) {
            official.copy(isFavorite = true)
        } else {
            official.copy(isFavorite = false)
        }
    }

    private suspend fun updateState(state: HomeViewState) {
        analyticService.newEvent(state)

        when (state) {
            is HomeViewState.Content -> _homeViewState.send(state)
            is HomeViewState.Error -> {
                _homeEffect.send(state)
                _homeViewState.send(HomeViewState.Content.ErrorContent(currentState()))
            }
            is HomeViewState.Effect -> _homeEffect.send(state)
            else -> throw IllegalStateException("Unknown state received")
        }
    }

    private fun currentState() = _homeViewState.value.data

    override fun onCleared() {
        if (!job.isCancelled) {
            job.cancel()
        }
        _homeViewState.cancel()
        _homeEffect.cancel()
    }
}