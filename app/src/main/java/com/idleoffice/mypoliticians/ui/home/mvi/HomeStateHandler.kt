/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home.mvi

import com.idleoffice.mypoliticians.ui.home.OfficialAdapter
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import timber.log.Timber

class HomeStateHandler(
    private val homeInteractor: HomeFragmentInteractor,
    private val officialAdapter: OfficialAdapter
) {
    suspend fun <E: HomeViewState?> observe(flow: Flow<E>) {
        flow.collectLatest {
            runCatching {
                handleState(it)
            }.onFailure {
                Timber.e(it, "Error occurred handling new state")
            }
        }
    }

    private fun handleContent(state: HomeViewState.Content) {
        homeInteractor.updateBindings(state.data)

        if (officialAdapter.officials != state.data.officials) {
            homeInteractor.scrollToPosition(0)
            officialAdapter.officials.clear()
            officialAdapter.officials.addAll(state.data.officials)

            officialAdapter.notifyDataSetChanged()
            homeInteractor.scheduleLayoutAnimation()
        }

        homeInteractor.setNoResultVisibility(state.data.officials.isEmpty())
    }

    private fun handleError(state: HomeViewState.Error) {
        Timber.e("Error home event: ${state.userMessage}")
        homeInteractor.showLongToast(state.userMessage ?: "Error please try again later")
    }

    @Synchronized
    private fun handleState(state: HomeViewState?) {
        state ?: return

        homeInteractor.displayLoading(false)
        // TODO it might be good to break this down into LCE handlers at a later point
        when (state) {
            is HomeViewState.Content.Loading -> homeInteractor.displayLoading(true)
            is HomeViewState.Error -> handleError(state)
            is HomeViewState.Content -> handleContent(state)
        }
    }
}