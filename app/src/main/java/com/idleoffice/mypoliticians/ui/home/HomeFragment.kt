/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.idleoffice.mypoliticians.R
import com.idleoffice.mypoliticians.databinding.FragmentHomeBinding
import com.idleoffice.mypoliticians.extension.setTouchable
import com.idleoffice.mypoliticians.ui.base.MviFragment
import com.idleoffice.mypoliticians.ui.home.mvi.HomeActor
import com.idleoffice.mypoliticians.ui.home.mvi.HomeFragmentInteractor
import com.idleoffice.mypoliticians.ui.home.mvi.HomeStateHandler
import com.idleoffice.mypoliticians.ui.home.mvi.HomeViewStateData
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.launch
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.core.qualifier.named

class HomeFragment : MviFragment() {

    private val homeViewModel: HomeViewModel by sharedViewModel()

    private val koinScope = getKoin().getOrCreateScope("home", named<HomeFragment>())
    private val officialAdapter: OfficialAdapter by koinScope.inject()

    private lateinit var binding: FragmentHomeBinding

    private lateinit var linearLayoutManager: LinearLayoutManager

    private val keyScrollPosition = "key_scroll_position"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        activity?.findViewById<FloatingActionButton>(R.id.favoriteFab)?.run {
            hide()
            setOnClickListener(null)
        }

        binding = FragmentHomeBinding.inflate(inflater, container, false)
        val actor = HomeActor(homeViewModel::takeAction)
        binding.actor = actor
        binding.data = HomeViewStateData()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView = requireActivity().findViewById<RecyclerView>(R.id.officialsList)
        startObservers(homeViewModel)
        initRecyclerView(recyclerView, officialAdapter, savedInstanceState)
    }

    private fun startObservers(viewModel: HomeViewModel) {

        val handler = HomeStateHandler(
            Interactor(),
            officialAdapter
        )

        mainScope.launch {
            handler.observe(viewModel.homeEffect())
        }

        mainScope.launch {
            handler.observe(viewModel.homeViewState)
        }
    }

    private fun initRecyclerView(
        recyclerView: RecyclerView,
        officialAdapter: OfficialAdapter,
        savedInstanceState: Bundle?
    ) {
        linearLayoutManager = LinearLayoutManager(context)

        savedInstanceState?.getParcelable<Parcelable?>(keyScrollPosition)?.let {
            linearLayoutManager.onRestoreInstanceState(it)
        }

        recyclerView.apply {
            itemAnimator = DefaultItemAnimator()
            adapter = officialAdapter
            layoutManager = linearLayoutManager
        }
    }

    // Private so we're not exposing the methods publicly without providing them
    private inner class Interactor : HomeFragmentInteractor {

        override fun displayLoading(shouldDisplay: Boolean) {
            view?.findViewById<SwipeRefreshLayout>(R.id.swipeRefresh)?.isRefreshing = shouldDisplay
            activity?.setTouchable(!shouldDisplay)
        }

        override fun setNoResultVisibility(shouldDisplay: Boolean) {
            view?.findViewById<TextView>(R.id.noResultsText)?.let {
                it.visibility = if (shouldDisplay) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
            }
        }

        override fun showLongToast(text: String) {
            this@HomeFragment.showLongToast(text)
        }

        override fun updateBindings(data: HomeViewStateData) {
            binding.data = data
        }

        override fun scrollToPosition(position: Int) {
            officialsList.scrollToPosition(position)
        }

        override fun scheduleLayoutAnimation() {
            officialsList.scheduleLayoutAnimation()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (::linearLayoutManager.isInitialized) {
            linearLayoutManager.onSaveInstanceState()?.let {
                outState.putParcelable(keyScrollPosition, it)
            }
        }
        super.onSaveInstanceState(outState)
    }
}