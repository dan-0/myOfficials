/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.base.mvi

import com.idleoffice.mypoliticians.analytics.AppEvent
import com.idleoffice.mypoliticians.model.data.Official

sealed class SearchResultState(event: String): AppEvent(event, "official_result") {
    class Content(val officials: List<Official>): SearchResultState("success")

    abstract class Error(errorEvent: String) : SearchResultState(errorEvent)
    class ValidationError(val errors: List<String>): Error("validation_error")
    object EmptyOfficialsError : Error("no_officials")
    class NetworkError(val statusCode: Int, val statusMsg: String?): Error("network_error")
    class CallFailure(val t: Throwable) : Error("call_failure")
    object NullBodyError : Error("null_body_error")
}