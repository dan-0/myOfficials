/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home.mvi

import android.content.res.Resources
import com.idleoffice.mypoliticians.R
import com.idleoffice.mypoliticians.analytics.AppEvent
import com.idleoffice.mypoliticians.model.data.Official

data class HomeViewStateData(
    val lastAddress: String = "",
    val officials: MutableList<Official> = mutableListOf()
)

sealed class HomeViewState(event: String) : AppEvent(event, "home_view_state") {

    sealed class Content(
        val data: HomeViewStateData,
        event: String
    ) : HomeViewState(event) {
        class Loading(state: HomeViewStateData) : Content(state, "home_view_state_loading")

        /**
         * Load Content state, necessary for clearing the Loading state
         */
        class ErrorContent(state: HomeViewStateData) : Content(state, "home_view_state_error")

        class Init(state: HomeViewStateData) : Content(state, "home_view_state_init")
        class OfficialsChanged(state: HomeViewStateData) :
            Content(state, "home_view_state_officials")

        class FavoritesChanged(state: HomeViewStateData) :
            Content(state, "home_view_state_favorites_changed")
    }

    abstract class Effect(event: String) : HomeViewState(event)

    object NewResultsLoaded : Effect("home_new_results")

    sealed class Error(
        event: String,
        val userMessage: String? = null
    ) : Effect(event) {
        class NoNetworkError(resources: Resources) :
            HomeViewState.Error("home_no_network", resources.getString(R.string.no_network_found))

        class SearchResultError(resources: Resources) :
            HomeViewState.Error(
                "home_network_error",
                resources.getString(R.string.search_result_error)
            )

        class UnknownError(resources: Resources) : HomeViewState.Error(
            "home_unknown_error", resources.getString(
                R.string.home_unknown_error
            )
        )

        class UnknownHostError(resources: Resources) : HomeViewState.Error(
            "home_unknown_host_error", resources.getString(
                R.string.home_unknown_error
            )
        )

        class EmptySearchText(resources: Resources) : HomeViewState.Error(
            "home_empty_search", resources.getString(R.string.home_empty_search_msg)
        )

        class ValidationError(resources: Resources) : HomeViewState.Error(
            "home_validation_error",
            resources.getString(R.string.validation_error)
        )
    }
}

