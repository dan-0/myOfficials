/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.official.mvi

import com.idleoffice.mypoliticians.analytics.AppEvent
import com.idleoffice.mypoliticians.model.data.Channel
import com.idleoffice.mypoliticians.model.data.Official
import com.idleoffice.mypoliticians.model.data.OfficialContactData
import java.util.*

sealed class OfficialState(event: String) : AppEvent(event, "official_event") {
    abstract class Content(val official: Official, tag: String) : OfficialState(tag)
    class Init(official: Official) : Content(official, "official_state_official_init")
    class UpdateFavorite(official: Official) : Content(official, "official_state_official_update")

    abstract class Event(tag: String) : OfficialState(tag)

    class OpenContact(val contactData: OfficialContactData) :
        Event("open_contact_data_${contactData.type.friendlyName}")

    class OpenSocialMedia(val socialMediaData: Channel) :
        Event("open_contact_channel_${socialMediaData.type?.friendlyName?.toLowerCase(Locale.ENGLISH)}")
}