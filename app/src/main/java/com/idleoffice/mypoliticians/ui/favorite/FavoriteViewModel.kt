/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.favorite

import androidx.lifecycle.ViewModel
import com.idleoffice.mypoliticians.analytics.AnalyticService
import com.idleoffice.mypoliticians.model.coroutines.getNewIoScope
import com.idleoffice.mypoliticians.model.data.Official
import com.idleoffice.mypoliticians.model.favorite.FavoritesProvider
import com.idleoffice.mypoliticians.ui.favorite.mvi.FavoriteAction
import com.idleoffice.mypoliticians.ui.favorite.mvi.FavoriteViewState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class FavoriteViewModel(
    private val analyticService: AnalyticService,
    private val favoritesProvider: FavoritesProvider,
    private val job: Job = SupervisorJob(),
    private val ioScope: CoroutineScope = getNewIoScope(job)
) : ViewModel() {

    private val _favoriteState = ConflatedBroadcastChannel<FavoriteViewState.Content>(FavoriteViewState.Content.Init())
    val favoriteState
        get() = _favoriteState.asFlow()

    init {
        subscribeToChannel()
    }

    private fun subscribeToChannel() {
        ioScope.launch {
            favoritesProvider.favoritesChannel.collect {
                if (it == _favoriteState.value.data) return@collect
                val newState = FavoriteViewState.Content.UpdateFavorite(it.sortOfficials())
                updateState(newState)
            }
        }
    }

    suspend fun takeAction(action: FavoriteAction) {
        analyticService.newAction(action)

        when (action) {
            is FavoriteAction.RemoveFavorite -> handleRemoveFavorite(action.official)
        }
    }

    private fun List<Official>.sortOfficials(): List<Official> {
        val list = toMutableList()
        list.sortBy { it.name.substringBefore(" ") }
        return list.toList()
    }

    private suspend fun handleRemoveFavorite(official: Official) {
        favoritesProvider.removeFavoriteOfficial(official)
    }

    private fun updateState(state: FavoriteViewState.Content) {
        analyticService.newEvent(state)
        _favoriteState.offer(state)
    }

    override fun onCleared() {
        _favoriteState.close()

        if (!job.isCancelled) {
            job.cancel()
        }
        super.onCleared()
    }
}