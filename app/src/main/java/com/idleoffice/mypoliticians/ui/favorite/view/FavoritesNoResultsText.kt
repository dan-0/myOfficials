/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.favorite.view

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ImageSpan
import android.util.AttributeSet
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.idleoffice.mypoliticians.R

/**
 * A [TextView] wrapper that allows adding an [ImageSpan] inline with the text
 */
class FavoritesNoResultsText @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : TextView(context, attrs, defStyleAttr) {

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val initText =
            SpannableString(resources.getString(R.string.no_favorite_results_message))

        val imageSpan = ContextCompat.getDrawable(context, R.drawable.favorite_image_span)!!.run {
            val imageHeight = lineHeight
            setBounds(0, 0, imageHeight, imageHeight)
            ImageSpan(this)
        }

        val end = initText.length
        initText.setSpan(imageSpan, end - 1, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        text = initText
    }
}