/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.official

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.idleoffice.mypoliticians.R
import com.idleoffice.mypoliticians.analytics.AnalyticService
import com.idleoffice.mypoliticians.databinding.ActivityOfficialBinding
import com.idleoffice.mypoliticians.model.data.*
import com.idleoffice.mypoliticians.ui.base.MviActivity
import com.idleoffice.mypoliticians.ui.official.mvi.OfficialActor
import com.idleoffice.mypoliticians.ui.official.mvi.OfficialState
import kotlinx.android.synthetic.main.activity_official.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class OfficialActivity : MviActivity() {

    private lateinit var officialViewModel: OfficialViewModel
    private lateinit var officialActor: OfficialActor

    private val analyticService: AnalyticService by inject()

    lateinit var binding: ActivityOfficialBinding

    companion object {
        const val OFFICIAL_DATA = "official_data_key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        officialViewModel = get { parametersOf(intent.getParcelableExtra(OFFICIAL_DATA)) }
        officialActor = OfficialActor(mainScope, officialViewModel::takeAction)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_official)

        binding.actor = officialActor

        setSupportActionBar(bottom_app_bar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        observeChanges()
    }

    private fun observeChanges() {
        defaultScope.launch {
            officialViewModel.officialState.collect {
                observeStateChanges(it)
            }
        }
        defaultScope.launch {
            officialViewModel.officialEvent.collect {
                observeStateChanges(it)
            }
        }
    }

    private fun observeStateChanges(state: OfficialState) {
        analyticService.newEvent(state)

        when (state) {
            is OfficialState.OpenContact -> handleOpenContact(state.contactData)
            is OfficialState.OpenSocialMedia -> handleSocialMediaOpen(state.socialMediaData)
            is OfficialState.Init -> {
                setupContactInfo(state.official)
                updateOfficialData(state.official)
            }
            is OfficialState.UpdateFavorite -> {
                updateOfficialData(state.official)
            }
        }
    }

    private fun updateOfficialData(official: Official) {
        binding.official = official
    }

    private fun handleOpenContact(contactData: OfficialContactData) {
        when (contactData.type) {
            ContactType.ADDRESS -> {
                startContactTypeAction(
                    Intent.ACTION_VIEW,
                    Uri.parse("geo:0,0?q=${contactData.value}")
                )
            }
            ContactType.PHONE -> {
                startContactTypeAction(Intent.ACTION_DIAL, Uri.parse("tel:${contactData.value}"))
            }
            ContactType.URL -> {
                startContactTypeAction(Intent.ACTION_VIEW, Uri.parse(contactData.value))
            }
            ContactType.EMAIL -> {
                startContactTypeAction(
                    Intent.ACTION_SENDTO,
                    Uri.parse("mailto:${contactData.value}")
                )
            }
        }
    }

    private fun startContactTypeAction(action: String, uri: Uri) {
        Intent(action, uri).apply { startActivity(this) }
    }

    private fun handleSocialMediaOpen(channel: Channel) {
        val intent = when (channel.type) {
            ChannelType.FACEBOOK -> openFacebook(channel)
            ChannelType.TWITTER -> openTwitter(channel)
            ChannelType.YOUTUBE -> openYoutube(channel)
            else -> {
                Timber.e("Invalid channel type: ${channel.type}")
                return
            }
        }

        intent?.let { startActivity(it) }
    }

    private fun openFacebook(channel: Channel): Intent {
        val uri = Uri.parse("https://facebook.com/${channel.id}")
        Timber.d("Opening Facebook: ${uri.encodedPath}")
        return Intent(Intent.ACTION_VIEW, uri)
    }

    private fun openTwitter(channel: Channel): Intent {
        val twitterEnabled = runCatching {
            packageManager.getApplicationInfo("com.twitter.android", 0).enabled
        }.getOrElse { false }

        val path = if (twitterEnabled) {
            "twitter://user?screen_name=${channel.id}"
        } else {
            "https://twitter.com/${channel.id}"
        }

        val uri = Uri.parse(path)

        Timber.d("Opening Twitter: ${uri.encodedPath}")
        return Intent(Intent.ACTION_VIEW, uri)
    }

    private fun openYoutube(channel: Channel): Intent? {
        channel.id ?: return null
        // hacky way to get best estimate if actual YouTube channel or username
        val path = if (channel.id.length == 24 && channel.id[0] == 'U') {
            "channel/${channel.id}"
        } else {
            "${channel.id}"
        }
        val uri = Uri.parse("https://youtube.com/$path")

        Timber.d("Opening YouTube: $uri")
        return Intent(Intent.ACTION_VIEW, uri)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> false
        }
    }

    private fun setupContactInfo(official: Official) {
        val contacts = mutableListOf(
            official.address,
            official.phones,
            official.emails,
            official.urls
        ).flatMap {
            it?.asIterable() ?: mutableListOf()
        }

        val contactAdapter = OfficialContactAdapter(contacts, officialActor)

        findViewById<RecyclerView>(R.id.officialContactList).run {
            itemAnimator = DefaultItemAnimator()
            adapter = contactAdapter

            val linearLayoutManager = LinearLayoutManager(context)
            layoutManager = linearLayoutManager

            val divider = DividerItemDecoration(context, linearLayoutManager.orientation)

            addItemDecoration(divider)
        }
    }
}