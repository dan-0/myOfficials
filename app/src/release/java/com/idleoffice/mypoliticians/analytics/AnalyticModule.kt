package com.idleoffice.mypoliticians.analytics

import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val analyticModule = module {
    single { FirebaseAnalyticService(androidApplication()) as AnalyticService }
}