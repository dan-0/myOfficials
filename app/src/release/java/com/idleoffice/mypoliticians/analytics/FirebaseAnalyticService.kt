package com.idleoffice.mypoliticians.analytics

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import timber.log.Timber

class FirebaseAnalyticService(context: Context) : AnalyticService {

    private val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

    override fun newEvent(event: AppEvent) {
        Timber.d("Received event: $event")
        Bundle().run {
            putString("feature", event.feature)
            putString("event", event.event)
            firebaseAnalytics.logEvent("event", this)
        }
    }

    override fun newAction(action: AppAction) {
        Timber.d("Received action: $action")
        Bundle().run {
            putString("feature", action.feature)
            putString("action", action.action)
            firebaseAnalytics.logEvent("action", this)
        }
    }
}