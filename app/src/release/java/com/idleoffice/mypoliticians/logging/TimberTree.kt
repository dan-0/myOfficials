package com.idleoffice.mypoliticians.logging

import android.util.Log
import com.crashlytics.android.Crashlytics
import timber.log.Timber

class TimberTree: Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        when(priority) {
            Log.ERROR -> {
                if(t != null) {
                    Crashlytics.logException(t)
                }
                Crashlytics.log(priority, tag, message)
            }
        }
    }
}