package com.idleoffice.mypoliticians.idlingresource

import androidx.test.espresso.IdlingResource
import org.koin.dsl.module

class ReleaseIdlingResources : AppIdlingResource {
    override fun getName() = "stub"

    override fun isIdleNow(): Boolean = true

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?) {
        /* Stub */
    }

    override fun start() {
        /* Stub */
    }

    override fun stop() {
        /* Stub */
    }
}

val idlingResourceModule = module {
    single { ReleaseIdlingResources() as AppIdlingResource }
}