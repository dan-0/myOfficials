/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.idlingresource

import androidx.test.espresso.IdlingResource
import androidx.test.espresso.idling.CountingIdlingResource
import com.idleoffice.mypoliticians.idlingresource.AppIdlingResource.Companion.APP_IDLING_RESOURCE
import org.koin.dsl.module
import timber.log.Timber

class DebugIdlingResources : AppIdlingResource {
    private val idlingResource = CountingIdlingResource(APP_IDLING_RESOURCE)

    override fun getName() = APP_IDLING_RESOURCE

    override fun isIdleNow() = idlingResource.isIdleNow

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?)
            = idlingResource.registerIdleTransitionCallback(callback)

    private var counter = 0

    override fun start() {
        idlingResource.increment()
        counter++
        Timber.d("Counter increment: $counter")
    }

    override fun stop() {
        idlingResource.decrement()
        counter--
        Timber.d("Counter decrement: $counter")
    }
}

val idlingResourceModule = module {
    single { DebugIdlingResources() as AppIdlingResource }
}