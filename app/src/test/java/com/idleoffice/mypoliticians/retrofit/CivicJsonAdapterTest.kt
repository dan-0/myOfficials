/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.retrofit

import com.idleoffice.mypoliticians.helpers.FakeOfficialResponseHelper
import com.idleoffice.mypoliticians.helpers.FakeResources
import com.idleoffice.mypoliticians.model.data.OfficialsResponse
import com.idleoffice.mypoliticians.model.transformer.transform
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class CivicJsonAdapterTest {

    private val fakeResources = FakeResources()
    private val ut = CivicJsonAdapter(fakeResources)

    @Test
    fun officialResultFromJson() {
        val officialsResponse = OfficialsResponse(
            offices = listOf(
                FakeOfficialResponseHelper.buildOfficeResponse(0),
                FakeOfficialResponseHelper.buildOfficeResponse(1)
            ),
            officialResponses = listOf(
                FakeOfficialResponseHelper.buildOfficialResponse(0),
                FakeOfficialResponseHelper.buildOfficialResponse(1)
            )
        )

        val expected = officialsResponse.transform(fakeResources)

        val result = ut.officialResultFromJson(officialsResponse)

        assertEquals(expected.event, result.event)
        assertEquals(expected.feature, result.feature)
    }
}