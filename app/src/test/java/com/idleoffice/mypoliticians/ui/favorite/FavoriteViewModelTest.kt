/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.favorite

import com.idleoffice.mypoliticians.helpers.ChannelObserver
import com.idleoffice.mypoliticians.helpers.FakeAnalyticService
import com.idleoffice.mypoliticians.helpers.FakeFavoriteProvider
import com.idleoffice.mypoliticians.helpers.FakeTestOfficials
import com.idleoffice.mypoliticians.ui.favorite.mvi.FavoriteAction
import com.idleoffice.mypoliticians.ui.favorite.mvi.FavoriteViewState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import me.danlowe.testutils.blockingTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class FavoriteViewModelTest {

    private lateinit var fakeFavoriteProvider: FakeFavoriteProvider
    private lateinit var fakeAnalyticService: FakeAnalyticService
    private lateinit var ut: FavoriteViewModel
    private lateinit var favoriteStateChannelObserver: ChannelObserver<FavoriteViewState>
    private lateinit var scopeJob: Job
    private lateinit var observerJob: Job


    private fun setUp(scope: CoroutineScope) {
        fakeFavoriteProvider = FakeFavoriteProvider()
        fakeAnalyticService = FakeAnalyticService()

        scopeJob = Job()
        ut = buildViewModel(CoroutineScope(scope.coroutineContext + scopeJob))
        favoriteStateChannelObserver = ChannelObserver()

        scope.launch {
            observerJob = favoriteStateChannelObserver.observeState(ut.favoriteState, scope)
        }
    }

    private fun cleanup() {
        observerJob.cancel()
        scopeJob.cancel()
    }

    @Test
    fun `with default values`() = blockingTest {
        ut = FavoriteViewModel(FakeAnalyticService(), FakeFavoriteProvider())

        val testJob = Job()
        val testScope = CoroutineScope(coroutineContext + testJob)
        val observer = ChannelObserver<FavoriteViewState>()
        observer.observeState(ut.favoriteState, testScope)

        assertEquals(
            FavoriteViewState.Content.Init::class,
            observer.lastState!!::class
        )

        testJob.complete()
    }

    @Test
    fun `initialization of FavoriteViewModel`() = blockingTest(::setUp, ::cleanup) {
        val currentState = favoriteStateChannelObserver.lastState

        assertEquals(
            FavoriteViewState.Content.Init::class,
            currentState!!::class,
            "Current state was ${currentState::class}"
        )
    }

    @Test
    fun `favorite officials updated`() = blockingTest(::setUp, ::cleanup) {
        val favoriteOfficial = FakeTestOfficials.basicFakeOfficials[0].copy(isFavorite = true)
        fakeFavoriteProvider.addFavoriteOfficial(favoriteOfficial)

        val currentState = favoriteStateChannelObserver.lastState

        assertTrue(
            currentState is FavoriteViewState.Content.UpdateFavorite
        )
    }

    @Test
    fun `handle remove official`() = blockingTest(::setUp, ::cleanup) {

        val favoriteOfficial = FakeTestOfficials.basicFakeOfficials[0].copy(isFavorite = true)
        fakeFavoriteProvider.addFavoriteOfficial(favoriteOfficial)

        val action = FavoriteAction.RemoveFavorite(favoriteOfficial)
        ut.takeAction(action)

        val currentState = favoriteStateChannelObserver.lastState

        assertTrue(
            currentState is FavoriteViewState.Content.UpdateFavorite
        )

        val state = currentState as FavoriteViewState.Content.UpdateFavorite

        assertTrue(
            state.data.isEmpty()
        )

        assertEquals(1, fakeAnalyticService.actions.size)
        assertEquals(action, fakeAnalyticService.actions[0])
    }

    private fun buildViewModel(scope: CoroutineScope): FavoriteViewModel {

        return FavoriteViewModel(
            fakeAnalyticService,
            fakeFavoriteProvider,
            ioScope = scope
        )
    }
}