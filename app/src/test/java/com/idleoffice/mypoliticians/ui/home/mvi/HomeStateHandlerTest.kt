/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home.mvi

import com.idleoffice.mypoliticians.helpers.FakeAnalyticService
import com.idleoffice.mypoliticians.helpers.FakeResources
import com.idleoffice.mypoliticians.helpers.FakeTestOfficials
import com.idleoffice.mypoliticians.ui.home.OfficialAdapter
import io.mockk.*
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import me.danlowe.testutils.blockingTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import timber.log.Timber

internal class HomeStateHandlerTest {

    private lateinit var ut: HomeStateHandler
    private lateinit var fakeHelper: FakeInteractor
    private lateinit var fakeAnalyticService: FakeAnalyticService
    private val officialAdapter: OfficialAdapter = mockk(relaxed = true)
    private lateinit var testChannel: Channel<HomeViewState?>

    private lateinit var tempChannel: Channel<String>

    private fun setup(scope: CoroutineScope) {
        clearAllMocks()
        fakeAnalyticService = FakeAnalyticService()
        tempChannel = Channel()

        officialAdapter

        fakeHelper = FakeInteractor()

        testChannel = Channel()
        ut = HomeStateHandler(
            fakeHelper,
            officialAdapter
        )

        scope.launch {
            ut.observe(testChannel.consumeAsFlow())
        }.invokeOnCompletion {
            it ?: return@invokeOnCompletion

            if (it !is CancellationException) {
                it.printStackTrace()
            }
        }
    }

    private fun cleanup() {
        testChannel.cancel()
    }

    @Test
    fun `Loading content happy path`() = blockingTest(::setup, ::cleanup) {
        val emptyState = HomeViewStateData()

        testChannel.send(HomeViewState.Content.Loading(emptyState))

        // Stops loading, then starts loading
        assertEquals(2, fakeHelper.displayLoadingCalls.size)

        // First is the stop loading call
        assertEquals(false, fakeHelper.displayLoadingCalls[0])

        // Second is the load call
        assertEquals(true, fakeHelper.displayLoadingCalls[1])
    }

    @Test
    fun `Error happy path`() = blockingTest(::setup, ::cleanup) {
        val resources = FakeResources()
        testChannel.send(HomeViewState.Error.UnknownHostError(resources))

        assertEquals(
            "Error retrieving results. Please check your address and network connection and try again later",
            fakeHelper.toastMessages[0]
        )
    }

    @Test
    fun `new Content happy path`() = blockingTest(::setup, ::cleanup) {

        every { officialAdapter.officials }.answers { mutableListOf() }

        val fakeOfficialsState = HomeViewStateData(officials = FakeTestOfficials.basicFakeOfficials)

        testChannel.send(HomeViewState.Content.Init(fakeOfficialsState))

        assertEquals(
            false,
            fakeHelper.showResultTextCalls[0]
        )

        assertEquals(0,
            fakeHelper.scrollToPositionCalls[0]
        )

        assertEquals(
            1,
            fakeHelper.scheduleLayoutAnimations.size
        )

        verify(exactly = 1) { officialAdapter.notifyDataSetChanged() }
    }

    @Test
    fun `same Content_Init happy path`() = blockingTest(::setup, ::cleanup) {
        val emptyState = HomeViewStateData()
        every { officialAdapter.officials }.answers { emptyState.officials }
        val contentState = HomeViewState.Content.Init(emptyState)

        testContentHappyPath(contentState)
    }

    @Test
    fun `same Content_ErrorContent happy path`() = blockingTest(::setup, ::cleanup) {
        val emptyState = HomeViewStateData()
        every { officialAdapter.officials }.answers { emptyState.officials }
        val contentState = HomeViewState.Content.ErrorContent(emptyState)

        testContentHappyPath(contentState)
    }

    @Test
    fun `same Content_OfficialsChanged happy path`() = blockingTest(::setup, ::cleanup) {
        val emptyState = HomeViewStateData()
        every { officialAdapter.officials }.answers { emptyState.officials }
        val contentState = HomeViewState.Content.OfficialsChanged(emptyState)

        testContentHappyPath(contentState)
    }

    @Test
    fun `same Content_FavoritesChanged happy path`() = blockingTest(::setup, ::cleanup) {
        val emptyState = HomeViewStateData()
        every { officialAdapter.officials }.answers { emptyState.officials }
        val contentState = HomeViewState.Content.FavoritesChanged(emptyState)

        testContentHappyPath(contentState)
    }

    private suspend fun testContentHappyPath(content: HomeViewState.Content) {
        testChannel.send(content)

        assertEquals(
            true,
            fakeHelper.showResultTextCalls[0]
        )

        verify(exactly = 0) { officialAdapter.notifyDataSetChanged() }
    }

    @Test
    fun `test normal channel error`() = blockingTest(::setup, ::cleanup) {
        val resources = FakeResources()
        val state = spyk(HomeViewState.Error.UnknownHostError(resources))

        var lastException: Throwable? = null

        Timber.plant(object : Timber.DebugTree() {
            override fun e(t: Throwable?, message: String?, vararg args: Any?) {
                lastException = t
            }
        })

        val expectedException = TestException()

        every { state.userMessage } throws(expectedException)

        testChannel.send(state)

        assertEquals(
            lastException,
            expectedException
        )

        assertEquals(
            0,
            fakeHelper.toastMessages.size
        )
    }

    @Test
    fun `null state`() = blockingTest(::setup, ::cleanup) {
        testChannel.send(null)

        assertEquals(
            0,
            fakeHelper.displayLoadingCalls.size
        )
    }

    private class TestException: Exception()

    private class FakeInteractor : HomeFragmentInteractor {
        val displayLoadingCalls = mutableListOf<Boolean>()
        val showResultTextCalls = mutableListOf<Boolean>()
        val toastMessages = mutableListOf<String>()
        val updatedBindings = mutableListOf<HomeViewStateData>()
        val scrollToPositionCalls = mutableListOf<Int>()
        val scheduleLayoutAnimations = mutableListOf<Int>()

        override fun displayLoading(shouldDisplay: Boolean) {
            displayLoadingCalls.add(shouldDisplay)
        }

        override fun setNoResultVisibility(shouldDisplay: Boolean) {
            showResultTextCalls.add(shouldDisplay)
        }

        override fun showLongToast(text: String) {
            toastMessages.add(text)
        }

        override fun updateBindings(data: HomeViewStateData) {
            updatedBindings.add(data)
        }

        override fun scrollToPosition(position: Int) {
            scrollToPositionCalls.add(position)
        }

        override fun scheduleLayoutAnimation() {
            scheduleLayoutAnimations.add(0)
        }
    }
}