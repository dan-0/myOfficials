/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.official.mvi

import com.idleoffice.mypoliticians.model.data.Channel
import com.idleoffice.mypoliticians.model.data.ChannelType
import com.idleoffice.mypoliticians.model.data.ContactType
import com.idleoffice.mypoliticians.model.data.OfficialContactData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import me.danlowe.testutils.blockingTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class OfficialActorTest {

    private lateinit var ut: OfficialActor
    private lateinit var testJob: Job
    private lateinit var fakeTakeAction: FakeTakeAction

    private fun setup(scope: CoroutineScope) {
        testJob = Job()
        val testScope = CoroutineScope(scope.coroutineContext + testJob)

        fakeTakeAction = FakeTakeAction()
        ut = OfficialActor(testScope, fakeTakeAction::takeAction)
    }

    private fun cleanup() {
        testJob.cancel()
    }

    @Test
    fun `clickContact happy path`() = blockingTest(::setup, ::cleanup) {
        val expectedContact = OfficialContactData(ContactType.ADDRESS, "123 Fake Street")

        ut.clickContact(expectedContact)

        assertEquals(
            OfficialAction.ContactInfoClicked::class,
            fakeTakeAction.receivedActions[0]::class
        )

        assertEquals(
            expectedContact,
            (fakeTakeAction.receivedActions[0] as OfficialAction.ContactInfoClicked).contactData
        )
    }

    @Test
    fun `clickSocialMedia happy path`() = blockingTest(::setup, ::cleanup) {
        val expectedChannel = Channel("fake_id", ChannelType.FACEBOOK)

        ut.clickSocialMedia(expectedChannel)

        assertEquals(
            OfficialAction.SocialMediaClicked::class,
            fakeTakeAction.receivedActions[0]::class
        )

        assertEquals(
            expectedChannel,
            (fakeTakeAction.receivedActions[0] as OfficialAction.SocialMediaClicked).channel
        )
    }

    @Test
    fun `clickFavorite happy path`() = blockingTest(::setup, ::cleanup) {
        ut.clickFavorite()

        assertEquals(
            OfficialAction.FavoriteClicked::class,
            fakeTakeAction.receivedActions[0]::class
        )
    }

    private class FakeTakeAction {
        val receivedActions = mutableListOf<OfficialAction>()

        suspend fun takeAction(action: OfficialAction) {
            receivedActions.add(action)
        }
    }
}