/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home.mvi

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class HomeActorTest {

    private lateinit var ut: HomeActor
    private lateinit var receiver: ActorReceiver

    @BeforeEach
    fun setUp() {
        receiver = ActorReceiver()

        ut = HomeActor(receiver::takeAction)
    }

    @Test
    fun getSearchAddress() {
        val fakeAddress = "123 Fake St"

        ut.searchAddress(fakeAddress)

        assertEquals(
            HomeAction.SearchAddress::class,
            receiver.lastAction!!::class
        )

        assertEquals(
            fakeAddress,
            (receiver.lastAction as HomeAction.SearchAddress).searchAddress
        )
    }

    @Test
    fun refresh() {
        ut.refresh()

        assertEquals(
            HomeAction.Refresh,
            receiver.lastAction
        )
    }

    private class ActorReceiver {
        var lastAction: HomeAction? = null

        fun takeAction(action: HomeAction) {
            lastAction = action
        }
    }
}