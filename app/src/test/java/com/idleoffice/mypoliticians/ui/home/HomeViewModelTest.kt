/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home

import android.content.SharedPreferences
import android.content.res.Resources
import com.idleoffice.mypoliticians.analytics.AnalyticService
import com.idleoffice.mypoliticians.helpers.*
import com.idleoffice.mypoliticians.helpers.FakeCivicRequestService
import com.idleoffice.mypoliticians.idlingresource.AppIdlingResource
import com.idleoffice.mypoliticians.model.favorite.FavoritesProvider
import com.idleoffice.mypoliticians.retrofit.CivicRequestService
import com.idleoffice.mypoliticians.ui.base.mvi.SearchResultState
import com.idleoffice.mypoliticians.ui.home.mvi.HomeAction
import com.idleoffice.mypoliticians.ui.home.mvi.HomeViewState
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.koin.test.KoinTest
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import kotlin.coroutines.coroutineContext

@ExperimentalCoroutinesApi
internal class HomeViewModelTest : KoinTest {

    private lateinit var fakeCivicRequestService: FakeCivicRequestService
    private lateinit var fakeFavoriteProvider: FakeFavoriteProvider
    private lateinit var fakeAnalyticService: FakeAnalyticService
    private lateinit var fakeSharedPreferences: FakeSharedPreferences
    private lateinit var fakeResources: FakeResources

    private val testAddress = "123 Fake Street, Wilmington, NY, 10001"

    @BeforeEach
    fun setUp() {
        fakeCivicRequestService = FakeCivicRequestService()
        fakeFavoriteProvider = FakeFavoriteProvider()
        fakeAnalyticService = FakeAnalyticService()
        fakeSharedPreferences = FakeSharedPreferences()
        fakeResources = FakeResources()

        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @Test
    fun `default constructor args`() = runBlockingTest {
        val ut = HomeViewModel(
            fakeCivicRequestService,
            fakeAnalyticService,
            fakeSharedPreferences,
            fakeFavoriteProvider,
            fakeResources,
            StubIdlingResource()
        )

        var lastContent: HomeViewState.Content? = null

        val collectJob = launch {
            ut.homeViewState.collect {
                lastContent = it
            }
        }

        assertEquals(
            HomeViewState.Content.Init::class,
            lastContent!!::class
        )

        collectJob.cancel()
    }

    @Test
    fun `init happy path`() = runBlockingTest {
        val (_, homeViewStateObserver) = buildTest()
        assertEquals(
            homeViewStateObserver.lastState!!::class,
            HomeViewState.Content.Init::class
        )
    }

    @Test
    fun `init with last address`() = runBlockingTest {
        fakeSharedPreferences.Editor().putString(HomeViewModel.HOME_ADDRESS_KEY, testAddress)

        val (_, homeViewStateObserver) = buildTest()

        val testOfficials = FakeTestOfficials.basicFakeOfficials

        fakeCivicRequestService.changeState(SearchResultState.Content(testOfficials))

        assertEquals(
            HomeViewState.Content.OfficialsChanged::class,
            homeViewStateObserver.lastState!!::class
        )

        assertEquals(testAddress, homeViewStateObserver.lastState!!.data.lastAddress)
    }

    @Test
    fun `takeAction SearchAddress happy path`() = runBlockingTest {
        val (ut, observer) = buildTest()

        val testAction = HomeAction.SearchAddress(testAddress)

        val testOfficials = FakeTestOfficials.basicFakeOfficials

        fakeCivicRequestService.changeState(SearchResultState.Content(testOfficials))

        ut.takeAction(testAction)

        assertEquals(testOfficials, observer.lastState!!.data.officials)
    }

    @Test
    fun `takeAction SearchAddress happy path with favorites`() = runBlockingTest {
        val testAction = HomeAction.SearchAddress(testAddress)

        val testOfficials = FakeTestOfficials.generateFakeOfficials(3)
        testOfficials[0] = testOfficials[0].copy(isFavorite = true)
        val favoriteOfficial = testOfficials[0]

        fakeCivicRequestService.changeState(SearchResultState.Content(testOfficials))
        fakeFavoriteProvider.addFavoriteOfficial(favoriteOfficial)

        val (ut, observer) = buildTest()

        ut.takeAction(testAction)

        assertEquals(favoriteOfficial, observer.lastState!!.data.officials[0])
    }

    @Test
    fun `takeAction SearchAddress empty`() = runBlockingTest {
        val (ut, homeStateObserver, homeEffectObserver) = buildTest()

        val testAddress = null
        val testAction = HomeAction.SearchAddress(testAddress)

        val testOfficials = FakeTestOfficials.basicFakeOfficials

        fakeCivicRequestService.changeState(SearchResultState.Content(testOfficials))

        ut.takeAction(testAction)

        assertEquals(
            HomeViewState.Error.EmptySearchText::class, homeEffectObserver.lastState!!::class
        )

        assertEquals(
            HomeViewState.Content.ErrorContent::class, homeStateObserver.lastState!!::class
        )
    }

    @Test
    fun `takeAction SearchAddress network UnknownHostException`() = runBlockingTest {
        val (ut, homeStateObserver, homeEffectObserver) = buildTest()

        val testAction = HomeAction.SearchAddress(testAddress)
        fakeCivicRequestService.throwException(UnknownHostException())

        ut.takeAction(testAction)

        assertEquals(
            HomeViewState.Error.UnknownHostError::class, homeEffectObserver.lastState!!::class
        )

        assertEquals(
            HomeViewState.Content.ErrorContent::class, homeStateObserver.lastState!!::class
        )
    }

    @Test
    fun `takeAction SearchAddress NetworkError`() = runBlockingTest {
        val (ut, _, homeEffectObserver) = buildTest()

        val testAction = HomeAction.SearchAddress(testAddress)
        fakeCivicRequestService.changeState(SearchResultState.NetworkError(404, null))

        ut.takeAction(testAction)

        assertEquals(
            HomeViewState.Error.SearchResultError::class, homeEffectObserver.lastState!!::class
        )
    }

    @Test
    fun `takeAction SearchAddress CallFailure`() = runBlockingTest {
        val (ut, _, homeEffectObserver) = buildTest()

        val testAction = HomeAction.SearchAddress(testAddress)
        fakeCivicRequestService.changeState(SearchResultState.CallFailure(Exception()))

        ut.takeAction(testAction)

        assertEquals(
            HomeViewState.Error.NoNetworkError::class, homeEffectObserver.lastState!!::class
        )
    }

    @Test
    fun `takeAction SearchAddress ValidationError`() = runBlockingTest {
        val (ut, _, homeEffectObserver) = buildTest()

        val testAction = HomeAction.SearchAddress(testAddress)
        fakeCivicRequestService.changeState(SearchResultState.ValidationError(listOf()))

        ut.takeAction(testAction)

        assertEquals(
            HomeViewState.Error.ValidationError::class, homeEffectObserver.lastState!!::class
        )
    }

    @Test
    fun `takeAction SearchAddress NullBodyError`() = runBlockingTest {
        val (ut, _, homeEffectObserver) = buildTest()

        val testAction = HomeAction.SearchAddress(testAddress)
        fakeCivicRequestService.changeState(SearchResultState.NullBodyError)

        ut.takeAction(testAction)

        assertEquals(
            HomeViewState.Error.ValidationError::class, homeEffectObserver.lastState!!::class
        )
    }

    @Test
    fun `takeAction SearchAddress unknown network exception`() = runBlockingTest {
        val (ut, homeStateObserver, homeEffectObserver) = buildTest()

        val testAction = HomeAction.SearchAddress(testAddress)
        fakeCivicRequestService.throwException(SocketTimeoutException())

        ut.takeAction(testAction)

        assertEquals(
            HomeViewState.Error.UnknownError::class, homeEffectObserver.lastState!!::class
        )

        assertEquals(
            HomeViewState.Content.ErrorContent::class, homeStateObserver.lastState!!::class
        )
    }

    @Test
    fun `takeAction SearchPreviousAddress happy path`() = runBlockingTest {
        val (ut, observer) = buildTest()

        val testAction = HomeAction.SearchPreviousAddress(testAddress)

        val testOfficials = FakeTestOfficials.basicFakeOfficials

        fakeCivicRequestService.changeState(SearchResultState.Content(testOfficials))

        ut.takeAction(testAction)

        assertEquals(
            testOfficials, observer.lastState!!.data.officials
        )
    }

    @Test
    fun `takeAction verify analytics called`() = runBlockingTest {
        val (ut, observer) = buildTest()

        val testAction = HomeAction.SearchAddress(testAddress)

        val testOfficials = FakeTestOfficials.basicFakeOfficials

        fakeCivicRequestService.changeState(SearchResultState.Content(testOfficials))

        ut.takeAction(testAction)

        assertEquals(
            testAction,
            fakeAnalyticService.actions.last()
        )

        assertEquals(
            testOfficials, observer.lastState!!.data.officials
        )
    }

    @Test
    fun `takeAction handle refresh`() = runBlockingTest {
        val (ut, observer) = buildTest()

        val searchAction = HomeAction.SearchAddress(testAddress)
        val refreshAction = HomeAction.Refresh

        val testOfficials = FakeTestOfficials.basicFakeOfficials

        fakeCivicRequestService.changeState(SearchResultState.Content(testOfficials))

        ut.takeAction(searchAction)
        ut.takeAction(refreshAction)

        assertEquals(2, fakeAnalyticService.actions.size)

        assertEquals(fakeAnalyticService.actions[0], searchAction)
        assertEquals(fakeAnalyticService.actions[1], refreshAction)

        assertEquals(testOfficials, observer.lastState!!.data.officials)
    }

    private fun buildHomeViewModel(
        civicRequestService: CivicRequestService = fakeCivicRequestService,
        analyticService: AnalyticService = fakeAnalyticService,
        sharedPreferences: SharedPreferences = fakeSharedPreferences,
        favoritesProvider: FavoritesProvider = fakeFavoriteProvider,
        resources: Resources = fakeResources,
        idlingResource: AppIdlingResource = StubIdlingResource(),
        job: Job = SupervisorJob(),
        testScope: CoroutineScope
    ): HomeViewModel = HomeViewModel(
        civicRequestService,
        analyticService,
        sharedPreferences,
        favoritesProvider,
        resources,
        idlingResource,
        job,
        testScope,
        testScope
    )

    private suspend fun <T> buildContentChannelObserver(
        channel: Flow<T>,
        scope: CoroutineScope
    ) = ChannelObserver<T>().apply {
        observeState(channel, scope)
    }


    private suspend fun buildTest(): TestSubject {
        val testScope = CoroutineScope(coroutineContext + SupervisorJob())
        val ut = buildHomeViewModel(testScope = testScope)
        val homeStateObserver = buildContentChannelObserver(ut.homeViewState, testScope)
        val homeEffectObserver = buildContentChannelObserver(ut.homeEffect(), testScope)

        return TestSubject(
            ut,
            homeStateObserver,
            homeEffectObserver
        )
    }

    private data class TestSubject(
        val ut: HomeViewModel,
        val homeStateObserver: ChannelObserver<HomeViewState.Content>,
        val homeEffectObserver: ChannelObserver<HomeViewState>
    )
}