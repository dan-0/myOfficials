/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.official

import com.idleoffice.mypoliticians.helpers.*
import com.idleoffice.mypoliticians.model.data.*
import com.idleoffice.mypoliticians.ui.official.mvi.OfficialAction
import com.idleoffice.mypoliticians.ui.official.mvi.OfficialState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import me.danlowe.testutils.blockingTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class OfficialViewModelTest {

    private lateinit var ut: OfficialViewModel
    private lateinit var fakeAnalyticService: FakeAnalyticService
    private lateinit var fakeFavoriteProvider: FakeFavoriteProvider
    private lateinit var initialOfficial: Official
    private lateinit var eventObserver: ChannelObserver<OfficialState>
    private lateinit var stateObserver: ChannelObserver<OfficialState.Content>
    private lateinit var testJob: Job

    private fun setUp(scope: CoroutineScope) {
        initialOfficial = FakeTestOfficials.singleFakeOfficial
        fakeAnalyticService = FakeAnalyticService()
        fakeFavoriteProvider = FakeFavoriteProvider()

        eventObserver = ChannelObserver()
        stateObserver = ChannelObserver()

        ut = OfficialViewModel(
            fakeAnalyticService,
            fakeFavoriteProvider,
            StubIdlingResource(),
            initialOfficial
        )

        testJob = Job()
        val testScope = CoroutineScope(scope.coroutineContext + testJob)

        scope.launch {
            eventObserver.observeState(ut.officialEvent, testScope)
        }

        scope.launch {
            stateObserver.observeState(ut.officialState, testScope)
        }
    }

    private fun cleanup() {
        testJob.cancel()
    }

    @Test
    fun `SocialMediaClicked happy path`() = blockingTest(::setUp, ::cleanup) {
        val socialMediaChannel = Channel("test_id", ChannelType.FACEBOOK)
        val action = OfficialAction.SocialMediaClicked(socialMediaChannel)

        ut.takeAction(action)

        assertEquals(
            OfficialState.OpenSocialMedia::class,
            eventObserver.lastState!!::class
        )

        assertEquals(
            socialMediaChannel,
            (eventObserver.lastState as OfficialState.OpenSocialMedia).socialMediaData
        )
    }

    @Test
    fun `ContactInfoClicked happy path`() = blockingTest(::setUp, ::cleanup) {
        val contactData = OfficialContactData(ContactType.ADDRESS, "123 Fake Street")
        val action = OfficialAction.ContactInfoClicked(contactData)

        ut.takeAction(action)

        assertEquals(
            OfficialState.OpenContact::class,
            eventObserver.lastState!!::class
        )

        assertEquals(
            contactData,
            (eventObserver.lastState as OfficialState.OpenContact).contactData
        )
    }

    @Test
    fun `Update Favorite on`() = blockingTest(::setUp, ::cleanup) {
        val expectedAction = OfficialAction.FavoriteClicked
        ut.takeAction(expectedAction)

        assertEquals(
            OfficialState.UpdateFavorite::class,
            stateObserver.lastState!!::class
        )

        assertEquals(1, fakeFavoriteProvider.officials.size)
        assertTrue(fakeFavoriteProvider.officials[0].isFavorite)
    }

    @Test
    fun `Update Favorite off`() = blockingTest(::setUp, ::cleanup) {
        val expectedAction = OfficialAction.FavoriteClicked
        ut.takeAction(expectedAction)
        ut.takeAction(expectedAction)

        assertEquals(
            OfficialState.UpdateFavorite::class,
            stateObserver.lastState!!::class
        )

        assertEquals(0, fakeFavoriteProvider.officials.size)

        testJob.cancel()
    }

    @Test
    fun `analytic called`() = blockingTest(::setUp, ::cleanup) {
        val expectedAction = OfficialAction.FavoriteClicked
        ut.takeAction(expectedAction)

        assertEquals(
            1,
            fakeAnalyticService.actions.size
        )

        assertEquals(
            expectedAction,
            fakeAnalyticService.actions[0]
        )
    }
}