/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.ui.home

import com.idleoffice.mypoliticians.helpers.FakeTestOfficials
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class OfficialClickedActorTest {

    @Test
    fun officialClicked() {
        var lastAction: OfficialOverviewAction? = null

        val ut = OfficialClickedActor {
            lastAction = it
        }

        val testOfficial = FakeTestOfficials.singleFakeOfficial

        ut.officialClicked(testOfficial)

        assertEquals(
            OfficialOverviewAction.Clicked::class,
            lastAction!!::class
        )

        assertEquals(
            testOfficial,
            (lastAction as OfficialOverviewAction.Clicked).official
        )
    }
}