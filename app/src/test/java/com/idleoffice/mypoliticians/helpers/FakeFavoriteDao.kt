/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.helpers

import com.idleoffice.mypoliticians.model.dao.FavoriteDao
import com.idleoffice.mypoliticians.model.data.Official

class FakeFavoriteDao : FavoriteDao {
    private val favoriteOfficials = mutableSetOf<Official>()

    override fun getAll(): List<Official> {
        return favoriteOfficials.toList()
    }

    override fun getById(id: Int) = favoriteOfficials.firstOrNull { it.id == id }

    override fun insertOfficial(official: Official) {
        favoriteOfficials.add(official)
    }

    override fun removeOfficial(official: Official) {
        favoriteOfficials.remove(official)
    }

    override fun updateOfficial(official: Official) {
        favoriteOfficials.removeIf { it.id == official.id }
        favoriteOfficials.add(official)
    }

    override fun deleteAllOfficials() {
        favoriteOfficials.clear()
    }
}