/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.helpers

import com.idleoffice.mypoliticians.model.data.Official
import com.idleoffice.mypoliticians.model.favorite.FavoritesProvider
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow

/**
 * A fake [FavoritesProvider] for testing
 */
class FakeFavoriteProvider : FavoritesProvider {
    val officials = mutableListOf<Official>()

    private val _favoritesChannel = BroadcastChannel<List<Official>>(Channel.CONFLATED)

    init {
        _favoritesChannel.sendBlocking(listOf())
    }

    override val favoritesChannel: Flow<List<Official>>
        get() = _favoritesChannel.asFlow()

    override suspend fun addFavoriteOfficial(official: Official) {
        officials.add(official)
        _favoritesChannel.send(officials)
    }

    override suspend fun removeFavoriteOfficial(official: Official) {
        officials.removeIf { official.id == it.id }
        _favoritesChannel.send(officials)
    }
}