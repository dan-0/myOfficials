/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.helpers

import android.content.res.Resources
import com.idleoffice.mypoliticians.R

/**
 * A FakeResources to use for testing.
 *
 * Note: Using the deprecated constructor due to a compiler issue that won't allow using the
 * ClassLoader based constructor
 */
@Suppress("DEPRECATION")
internal class FakeResources(
    private val defaultString: String = "Default test string"
) : Resources(null, null, null) {
    override fun getString(id: Int): String {
        return when (id) {
            R.string.political_party_unknown -> "Unknown Political Party"
            R.string.home_empty_search_msg -> "Must enter an address!"
            R.string.home_unknown_error -> "Error retrieving results. Please check your address and network connection and try again later"
            else -> defaultString
        }
    }
}