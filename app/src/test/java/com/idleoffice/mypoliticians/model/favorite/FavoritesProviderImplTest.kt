/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.favorite

import androidx.test.espresso.IdlingResource
import com.idleoffice.mypoliticians.helpers.FakeFavoriteDao
import com.idleoffice.mypoliticians.helpers.FakeTestOfficials
import com.idleoffice.mypoliticians.idlingresource.AppIdlingResource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
internal class FavoritesProviderImplTest {

    private lateinit var ut: FavoritesProviderImpl
    private lateinit var fakeFavoriteDao: FakeFavoriteDao
    private val fakeIdlingResource: AppIdlingResource = object : AppIdlingResource {
        // Stub everything, we don't need an idling resource for this
        override fun start() {}

        override fun stop() {}
        override fun getName() = ""
        override fun isIdleNow() = true
        override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?) {}
    }

    @BeforeEach
    fun setUp() {
        fakeFavoriteDao = FakeFavoriteDao()
    }

    @Test
    fun addFavoriteOfficial() = runBlockingTest {

        ut = FavoritesProviderImpl(
            fakeFavoriteDao,
            fakeIdlingResource
        )

        val testOfficial = FakeTestOfficials.basicFakeOfficials[0]

        val officials = runBlocking {
            ut.addFavoriteOfficial(testOfficial)
            ut.favoritesChannel.first()
        }

        assertEquals(1, officials.size)
        assertEquals(testOfficial, officials[0])
    }

    @Test
    fun removeFavoriteOfficial() = runBlockingTest {
        ut = FavoritesProviderImpl(
            fakeFavoriteDao,
            fakeIdlingResource
        )

        val testOfficial = FakeTestOfficials.basicFakeOfficials[0]

        val channel = ut.favoritesChannel
        runBlocking {
            ut.addFavoriteOfficial(testOfficial)
            ut.removeFavoriteOfficial(testOfficial)
        }

        assertEquals(0, channel.first().size)

    }
}