/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.db

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.os.Parcelable
import com.idleoffice.mypoliticians.helpers.FakeFavoriteDao
import com.idleoffice.mypoliticians.helpers.FakeTestOfficials
import com.idleoffice.mypoliticians.model.dao.FavoriteDao
import com.idleoffice.mypoliticians.model.db.FavoriteResolver.Companion.OFFICIAL_FAVORITE_KEY
import com.idleoffice.mypoliticians.model.wrappers.LocalBroadcastManagerWrapper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@ExperimentalCoroutinesApi
internal class FavoriteResolverTest {

    private lateinit var ut: FavoriteResolver.FavoriteChangeReceiver
    private lateinit var fakeFavoriteDao: FavoriteDao
    private val fakeBroadcastManager = FakeLocalBroadcastManagerWrapper()

    @BeforeEach
    fun setUp() {
        fakeFavoriteDao = FakeFavoriteDao()
        ut = FavoriteResolver.FavoriteChangeReceiver(fakeFavoriteDao)
    }

    @Test
    fun `receiver registered on instantiation`() {
        FavoriteResolver(fakeFavoriteDao, fakeBroadcastManager)

        assertEquals(1, fakeBroadcastManager.timesRegisterCalled)
    }

    @Test
    fun `onReceive null intent`() {
        ut.onReceive(null, null)

        assertEquals(0, fakeFavoriteDao.getAll().size, "DAO should not have had any modifications")
    }

    @Test
    fun `onReceive intent with no official`() {
        val intent = Intent()

        assertThrows<KotlinNullPointerException>("") {
            ut.onReceive(null, intent)
        }
    }

    @Test
    fun `onReceive with favorite official`() = runBlockingTest{
        ut = FavoriteResolver.FavoriteChangeReceiver(fakeFavoriteDao, this)
        val expectedOfficial = FakeTestOfficials.generateFakeOfficials(1)[0]
            .copy(isFavorite = true)

        val mockIntent: Intent = FakeIntent()
        mockIntent.putExtra(OFFICIAL_FAVORITE_KEY, expectedOfficial)

        ut.onReceive(null, mockIntent)

        assertEquals(1, fakeFavoriteDao.getAll().size)

        val addedOfficial = fakeFavoriteDao.getAll()[0]

        assertEquals(expectedOfficial, addedOfficial)
    }

    @Test
    fun `onReceive with not favorite official`() = runBlockingTest{
        ut = FavoriteResolver.FavoriteChangeReceiver(fakeFavoriteDao, this)

        val expectedOfficial = FakeTestOfficials.generateFakeOfficials(1)[0]
            .copy(isFavorite = false)

        val fakeIntent: Intent = FakeIntent()
        fakeIntent.putExtra(OFFICIAL_FAVORITE_KEY, expectedOfficial)

        fakeFavoriteDao.insertOfficial(expectedOfficial)

        ut.onReceive(null, fakeIntent)

        assertEquals(0, fakeFavoriteDao.getAll().size, "Official not properly removed")
    }

    private class FakeLocalBroadcastManagerWrapper : LocalBroadcastManagerWrapper {
        var timesRegisterCalled = 0
            private set

        override fun registerReceiver(
            broadcastReceiver: BroadcastReceiver,
            intentFilter: IntentFilter
        ) {
            timesRegisterCalled++
        }

        override fun unregisterReceiver(receiver: BroadcastReceiver) {
            throw NotImplementedError()
        }

    }

    private class FakeIntent : Intent() {
        private val map = hashMapOf<String?, Any?>()
        override fun putExtra(name: String?, value: Parcelable?)= apply {
            map[name] = value
        }

        override fun <T : Parcelable?> getParcelableExtra(name: String?): T {
            // Ok to cast here, it should never be anything different
            @Suppress("UNCHECKED_CAST")
            return map[name] as T
        }
    }
}