/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.typeconverter

import com.idleoffice.mypoliticians.model.data.Channel
import com.idleoffice.mypoliticians.model.data.ChannelType
import com.idleoffice.mypoliticians.model.data.ContactType
import com.idleoffice.mypoliticians.model.data.OfficialContactData
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

internal class OfficialTypeConverterTest {

    private val ut = OfficialTypeConverter()

    private val testContactData = listOf(
        OfficialContactData(ContactType.EMAIL, "test@google.com"),
        OfficialContactData(ContactType.PHONE, "202-123-4567"),
        OfficialContactData(ContactType.ADDRESS, "123 Fake St, Wilmington, VA 12345")
    )

    private val testContactJson = """[{"type":"EMAIL","value":"test@google.com"},{"type":"PHONE","value":"202-123-4567"},{"type":"ADDRESS","value":"123 Fake St, Wilmington, VA 12345"}]"""

    private val testChannel = Channel("test_id", ChannelType.FACEBOOK)

    private val testChannelJson = """{"id":"test_id","type":"FACEBOOK"}"""

    @Test
    fun `listContactDataToString happy path`() {
        val result = ut.listContactDataToString(testContactData)

        assertEquals(testContactJson, result)
    }

    @Test
    fun `listContactDataToString null input`() {
        val result = ut.listContactDataToString(null)
        assertNull(result)
    }

    @Test
    fun `stringToContactData happy path`() {
        val result = ut.stringToContactData(testContactJson)
        assertEquals(testContactData, result)
    }

    @Test
    fun `stringToContactData null input`() {
        val result = ut.stringToContactData(null)
        assertNull(result)
    }

    @Test
    fun `channelToString happy path`() {
        val result = ut.channelToString(testChannel)
        assertEquals(testChannelJson, result)
    }

    @Test
    fun `channelToString null input`() {
        val result = ut.channelToString(null)
        assertNull(result)
    }

    @Test
    fun `stringToChannel happy path`() {
        val result = ut.stringToChannel(testChannelJson)
        assertEquals(testChannel, result)
    }

    @Test
    fun `stringToChannel null input`() {
        val result = ut.stringToChannel(null)
        assertNull(result)
    }
}