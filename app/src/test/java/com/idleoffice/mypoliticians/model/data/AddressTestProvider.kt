/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.data

object AddressTestProvider {

    val baseTestAddressResponse = AddressResponse(
        "Fairfax",
        "123 Fake St",
        "Apt 1",
        "C/O: Bob",
        "Main Office",
        "VA",
        "10000"
    )

    /**
     * Returns an [AddressResponse] where all values end in [id]
     */
    fun getIndexedAddress(id: Int) = AddressResponse(
        "Fairfax$id",
        "123 Fake St$id",
        "Apt 1$id",
        "C/O: Bob$id",
        "Main Office$id",
        "VA$id",
        "10000$id"
    )
}