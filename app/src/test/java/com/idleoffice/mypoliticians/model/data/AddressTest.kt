/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.data

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class AddressTest {

    @Test
    fun `to friendly address happy path`() {
        val expectedAddress = "Main Office, 123 Fake St, Apt 1, C/O: Bob, Fairfax, VA, 10000"

        val actualAddress = AddressTestProvider.baseTestAddressResponse.toFriendlyAddress()

        assertEquals(expectedAddress, actualAddress)
    }

    @Test
    fun `empty address values`() {
        val expectedAddress = null

        val actualAddress = AddressResponse(
            null,
            null,
            null,
            null,
            null,
            null,
            null
        ).toFriendlyAddress()

        assertEquals(expectedAddress, actualAddress)
    }

    @Test
    fun `single non-null value in response`() {
        val expectedAddress = "Washington"

        val actualAddress = AddressResponse(
            "Washington",
            null,
            null,
            null,
            null,
            null,
            null
        ).toFriendlyAddress()

        assertEquals(expectedAddress, actualAddress)
    }
}