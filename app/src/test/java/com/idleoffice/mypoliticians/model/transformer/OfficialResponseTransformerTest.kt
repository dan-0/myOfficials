/*
 * My Officials
 * Copyright (C) 2019  Dan Lowe (support@idleoffice.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.idleoffice.mypoliticians.model.transformer

import com.idleoffice.mypoliticians.helpers.FakeOfficialResponseHelper.buildOfficeResponse
import com.idleoffice.mypoliticians.helpers.FakeOfficialResponseHelper.buildOfficialResponse
import com.idleoffice.mypoliticians.helpers.FakeResources
import com.idleoffice.mypoliticians.model.data.*
import com.idleoffice.mypoliticians.ui.base.mvi.SearchResultState
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class OfficialResponseTransformerTest {

    private val baseOfficialsResponse = OfficialsResponse(
        offices = listOf(
            buildOfficeResponse(0),
            buildOfficeResponse(1)
        ),
        officialResponses = listOf(
            buildOfficialResponse(0),
            buildOfficialResponse(1)
        )
    )

    private val expectedOfficial0 = Official(
        id = 1443309629,
        name = "Test User0",
        office = "Office0",
        party = "Test Party0",
        address = listOf(
            OfficialContactData(
                ContactType.ADDRESS,
                "Main Office0, 123 Fake St0, Apt 10, C/O: Bob0, Fairfax0, VA0, 100000"
            )
        ),
        phones = listOf(OfficialContactData(ContactType.PHONE, "202-123-4567")),
        emails = listOf(OfficialContactData(ContactType.EMAIL, "testuser0@gmail.com")),
        urls = listOf(OfficialContactData(ContactType.URL, "https://testuser.com/0")),
        twitterChannel = null,
        facebookChannel = Channel("test_user0", type = ChannelType.FACEBOOK),
        youtubeChannel = null,
        photoUrl = "https://google.com/0",
        isFavorite = false
    )

    private val expectedOfficial1 = Official(
        id = -1108847331,
        name = "Test User1",
        office = "Office1",
        party = "Test Party1",
        address = listOf(
            OfficialContactData(
                ContactType.ADDRESS,
                "Main Office1, 123 Fake St1, Apt 11, C/O: Bob1, Fairfax1, VA1, 100001"
            )
        ),
        phones = listOf(OfficialContactData(ContactType.PHONE, "202-123-4567")),
        emails = listOf(OfficialContactData(ContactType.EMAIL, "testuser1@gmail.com")),
        urls = listOf(OfficialContactData(ContactType.URL, "https://testuser.com/1")),
        twitterChannel = null,
        facebookChannel = Channel("test_user1", type = ChannelType.FACEBOOK),
        youtubeChannel = null,
        photoUrl = "https://google.com/1",
        isFavorite = false
    )

    private val fakeResources = FakeResources()

    @Test
    fun `officials response to official happy path`() =
        testSearchResult(baseOfficialsResponse) { ut ->
            val expectedOfficials = 2

            assertEquals(expectedOfficials, ut.officials.size)
            assertEquals(expectedOfficial0, ut.officials[0])
            assertEquals(expectedOfficial1, ut.officials[1])
        }

    @Test
    fun `empty officials response`() =
        testSearchResult(OfficialsResponse(listOf(), listOf())) { ut ->
            assertEquals(0, ut.officials.size)
        }

    @Test
    fun `empty office response with an official`() = testSearchResult(
        OfficialsResponse(listOf(), listOf(buildOfficialResponse(0)))
    ) { ut ->
        assertEquals(0, ut.officials.size)
    }

    @Test
    fun `empty official response with indexed offices`() {
        val response = baseOfficialsResponse.copy(officialResponses = null)

        assertTrue(response.transform(fakeResources) is SearchResultState.EmptyOfficialsError)
    }

    @Test
    fun `null OfficeRespones and OfficialResponses`() {
        val response = OfficialsResponse(null, null)
        testSearchResult(response) {
            assertEquals(0, it.officials.size)
        }
    }

    private fun testSearchResult(
        response: OfficialsResponse,
        func: (ut: SearchResultState.Content) -> Unit
    ) {
        val result = response.transform(fakeResources)
        assertTrue(result is SearchResultState.Content)
        func(result as SearchResultState.Content)
    }
}