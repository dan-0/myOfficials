-optimizationpasses 2
-allowaccessmodification
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-android
-verbose

### Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

-keep class com.idleoffice.mypoliticians.analytics.** { *; }

-keepclassmembers class com.idleoffice.mypoliticians.ui.base.mvi.** {
  <init>(...);
  <fields>;
}

-keep class com.idleoffice.mypoliticians.model.data.** { *; }

-dontwarn org.jetbrains.annotations.**
-keep class kotlin.Metadata { *; }